#include <mpi.h>
#include <unistd.h>
#include <bits/stdc++.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/stat.h>
#include <getopt.h>
#include <unistd.h>
using namespace std;

// process status
#define READ_READY 		0
#define READY_FOR_FILE	1
#define PROCESS_DONE 	-1
#define PROCESS_ERROR	-2
#define PROCESS_EXIT	-3
#define FILE_NOT_EXIST   -4
#define FILE_DAMAGE     -5

// #define path = dir + file name
#define FILE_DIR_LENGTH 1024
#define COMMD_LENGTH 1024

// time related
#define TIME_INTERVAL	5			// every time_interval, the manager tries to contact workers
#define TIME_COMMUNICATE_DEADLINE_LINE	TIME_INTERVAL*10 // consider lose contact
#define TIME_RELAX		3
#define TIME_MIN_FOR_PROCESSING	50	// within this time is considerred not finishing process
#define TIME_MAX_FOR_PROCESSING	120	// after this time is considerred process for too long
#define TIMES_FOR_RETRY	5			// the times that manager trys to contact workers

// tag for MPI communication
#define HEART_BEAT_TAG	1
#define SEND_RECV_TAG	0

// for debug
#define THREAD_DEBUG 				false
#define HEART_BEAT_STATUS_DEBUG 	false
#define MANAGER_DEBUG				false
#define WORKER_DEBUG				false
#define TEST_CODE					false

/*
	store file information
*/
struct FileInfo{
	string file_path;
	bool has_touched;	// whether this file has been processed
	bool is_error;		// whether error occured when processing this file
	FileInfo(string _file_path="", bool _has_touched=false, bool _is_error=false): file_path(_file_path), has_touched(_has_touched), is_error(_is_error){}
};

/*
	store workers infomation
*/
struct WorkerInfo{
	int worker_rank;
	FileInfo file_info;
	long last_communicate_time;	// for heartbeat test
	long assign_time;			// when manager assign jobs to this worker
	bool is_PINGed_jobs;		// whether has been called by manager
	MPI_Request request_jobs;	// used for MPI_Irecv() 
	bool is_PINGed_heartbeat;	// whether has been called by manager
	MPI_Request request_heartbeat;
	int process_status;
	WorkerInfo(int _worker_rank, string _file_path="", long _last_communicate_time=time(NULL), long _assign_time=0, bool _is_PINGed_jobs=false, bool _is_PINGed_heartbeat=false, int _process_status=10): worker_rank(_worker_rank), last_communicate_time(_last_communicate_time), assign_time(_assign_time), is_PINGed_jobs(_is_PINGed_jobs), is_PINGed_heartbeat(_is_PINGed_heartbeat), process_status(_process_status){
		file_info.file_path = _file_path;
	}
};

queue<FileInfo> files_queue;			// record files to process
queue<int> workers_pool;				// a queue of workers ID
vector<pthread_mutex_t> mutex_locks;	// used when access assign_time in WorkerInfo
vector<WorkerInfo> workers;				// running workers

char stored_file_node[20] = "TJU-FAST-RFI";			// used for remote file storage center
char dir_to_process[FILE_DIR_LENGTH] = "data_set";  // dir in which there are files to be processed
char dir_to_store_result[FILE_DIR_LENGTH] = "data_set/processed_results";
char dir_of_model[FILE_DIR_LENGTH] = "data_set/saved_model"; // trained model of RFI-Net
char tmp_file_dir_for_process[FILE_DIR_LENGTH] = "";
char workflow_organizer[FILE_DIR_LENGTH] = "workflow.py";
char py_interpreter[FILE_DIR_LENGTH] = "~/anaconda3/bin/python";

int files_num_to_process = 0;	// only used for test
int my_rank; 					// current process ID
int number_process; 			// total number of processes
bool thread_terminate = true;	// mark whether to end thread

// write log
ofstream error_log("error.log");
ofstream run_log("run.log");

WorkerInfo worker_self(-1);		// privately used for each worker itself

// methods
void* heartbeatTestManager(void* rank);
void* heartbeatTestWorker(void* rank);
char* getCtime();
int handelFile(const char* file_path);
int searchFile(const char* directory, const char* suffix, bool recursive, int subdir_level);
int getArgument(const int argc, char *argv[]);
int sendFilePath(const int worker_id);
int managerRoutine();
int workerRoutine();

/*
	returns format string of time, like "Wed Jul 29 22:15:29 2020"
*/
char* getCtime()
{
	time_t now_time = time(NULL);
	static char time_string[26] = {'\0'};
	strncpy(time_string, ctime(&now_time), 26);
	time_string[24] = '\0';
	return time_string;
}

/*
	manager assign file`s path to a worker, and record file info to worker info.
	input:		worker_id, the num of worker to assign file`s path
	output:		error log and run log of files if a worker is finished
*/
int sendFilePath(const int worker_id)
{
	int process_status;

	// if there are files to process, send file path to worker;
	// otherwise, inform worker to finish
	if(!files_queue.empty()){
		process_status = READY_FOR_FILE;
		MPI_Send(&process_status, 1, MPI_INT, worker_id, SEND_RECV_TAG, MPI_COMM_WORLD);

		char file_path[FILE_DIR_LENGTH];
		strncpy(file_path, files_queue.front().file_path.c_str(), FILE_DIR_LENGTH);
		printf("%s\tManager is assigning %s to worker %d\n", getCtime(), file_path, worker_id);
		MPI_Send(file_path, FILE_DIR_LENGTH, MPI_CHAR, worker_id, SEND_RECV_TAG, MPI_COMM_WORLD);

		workers[worker_id].file_info = files_queue.front();
		workers[worker_id].assign_time = time(NULL);

		files_queue.pop();
		workers_pool.push(worker_id);
	}
	else{
		process_status = PROCESS_DONE;
		printf("%s\tManager is sending jobs done to worker %d\n", getCtime(), worker_id);
		MPI_Send(&process_status, 1, MPI_INT, worker_id, SEND_RECV_TAG, MPI_COMM_WORLD);

		run_log<<getCtime()<<"\tWorker "<<worker_id<<" is done working"<<endl;
	}

	return 0;
}

/*
   	search files with specific suffix in a folder
   	input:	directory, the based folder to search files
			suffix, pattern to search file
			recursive, whether to search files in subdir
   			subdir_level, to avoid endless recursion
	output:	record files path into files_queue
*/
int searchFile(const char* directory, const char* suffix, bool recursive=false, int subdir_level=100)
{
    const char* dir_path = directory;
    DIR *dir = NULL;
    int file_num_count = 0;
     
    //  open a directory
    if((dir = opendir(dir_path)) == NULL)
    {
        return false;
    }
     
    struct dirent   entry;
    struct dirent*  result;
    struct stat     file_stat;
     
    char cur_name[FILE_DIR_LENGTH];
     
    //  read a file in directory
    while(readdir_r(dir, &entry, &result) == 0 && result)
    {
        const char* name = entry.d_name;
         
        //  ignore . and ..
        if(strcmp(name, ".") == 0 || strcmp(name, "..") == 0)
            continue;
         
        int count = sprintf(cur_name, "%s", dir_path);
        count += sprintf(&cur_name[count], "%s%s", cur_name[count - 1] == '/' ? "" : "/", name);
         
        // read status of a file
        if(lstat(cur_name, &file_stat) != -1)
        {
            //  if is a file
            if(!S_ISDIR(file_stat.st_mode))
            {  
                const char* rf = strrchr(cur_name, '.');
                 
                //  check suffix
                if(rf != NULL && strcmp(rf + 1, suffix) == 0){
                    file_num_count++;
                    files_queue.push(FileInfo(string(cur_name, count)));
                }
            }
            else if(recursive && subdir_level >= 0) // if is a folder
            {
                //  retrieves the files of the next level directory recursively 
                file_num_count += searchFile(cur_name, suffix, recursive, subdir_level - 1);
            }
        }
    }
    closedir(dir);
     
    return file_num_count;
}

/*
   	jobs of manager, including search files, heartbeat monitor workers,
	assign jobs to them receive responds and write log
	output:		record run log
*/
int managerRoutine()
{
	int thread_count = 1;
	MPI_Status status;

	#if TEST_CODE
		// Only for test, push file to queue
		for(int i = 0; i < files_num_to_process; i++){
			char file_dir[FILE_DIR_LENGTH];
			sprintf(file_dir, "Dec+0551_drifting-M19_W_00%d.fits", 18 + i);
			string file_dir_s(file_dir);
			FileInfo fileInfo(file_dir);
			files_queue.push(fileInfo);
			// files_queue.push("Dec+0551_drifting-M19_W_0018.fits");
		}
	#else
		cout<<getCtime()<<"\tfind "<<searchFile(dir_to_process, "fits", true, 100)<<" files "<<endl;
		// while (!files_queue.empty())
		// {
		// 	cout<<files_queue.front().file_path<<endl;
		// 	files_queue.pop();
		// }
	#endif

	// create dir to store result
	char commd[COMMD_LENGTH];
	sprintf(commd, "mkdir -p %s", dir_to_store_result);
	system(commd);

	// heart beat thread start
	mutex_locks.resize(number_process);
	pthread_t* thread_handles = NULL;
	thread_handles = (pthread_t*)malloc(thread_count * sizeof(pthread_t));
	thread_terminate = false;

	for(int i = 0; i < number_process; i++){
		WorkerInfo tmp_worker(i);
        workers.push_back(tmp_worker);
		pthread_mutex_init(&mutex_locks[i], NULL);
		// pthread_create(&thread_handles[i], NULL, hello_sleep, (void*)(long)i);
    }

	for(int i = 0; i < thread_count; i++){
		// pthread_mutex_init(&mutex_locks[i], NULL);
		pthread_create(&thread_handles[i], NULL, heartbeatTestManager, (void*)(long)i);
    }

	// set up workers pool
    for(int i = 1; i < number_process; i++){
        workers_pool.push(i);
    }

	// assign task to workers, deal with report from workers
    while(!workers_pool.empty()){
        int worker_id = workers_pool.front();
        int flag = 0;
		time_t last_communicate_time = 0, now_time = time(NULL);
        workers_pool.pop();

		// if running time is less than assumed process time,
		// it is considered that processing is not finish,
		// so will not recv the responds
		if(now_time - workers[worker_id].assign_time < TIME_MIN_FOR_PROCESSING){
			workers_pool.push(worker_id);
			
			#if HEART_BEAT_STATUS_DEBUG || MANAGER_DEBUG
				printf("%s\tTime is not up, pushing %d back to queue. now time %s, ass_time %s, diff %ld\n", getCtime(), worker_id, ctime(&now_time), ctime(&workers[worker_id].assign_time), now_time - workers[worker_id].assign_time);
			#endif
			
			// dynamically adjust time waiting for workers to finish processing
			sleep(10 + (TIME_MIN_FOR_PROCESSING - now_time + workers[worker_id].assign_time) / number_process);
			continue;
		}

		// if the worker is not responding for a long enough time,
		// the worker is considered not run normaly,
		// so it will be termimnated
		pthread_mutex_lock(&mutex_locks[worker_id]);
		last_communicate_time = workers[worker_id].last_communicate_time;
		pthread_mutex_unlock(&mutex_locks[worker_id]);

		if(now_time - last_communicate_time > TIME_COMMUNICATE_DEADLINE_LINE){
			// will not push worker_id back to the queue
			// but push file back to file_queue
			files_queue.push(workers[worker_id].file_info);

			// termiinate the worker
			workers[worker_id].process_status = PROCESS_DONE;
			MPI_Send(&workers[worker_id].process_status, 1, MPI_INT, worker_id, SEND_RECV_TAG, MPI_COMM_WORLD);
			
			error_log<<getCtime()<<"\tWorker "<<worker_id<<" has been termimnated since not responding"<<endl;
			
			#if HEART_BEAT_STATUS_DEBUG || MANAGER_DEBUG
				printf("%s\tWorker %d is not responding, might be down so will be terminated\n", getCtime(), worker_id);
			#endif

			continue;
		}

		// if running time is more than assumed process time,
		// processing is considered to somehow run unnormaly,
		// so it will be terminated
		if(workers[worker_id].assign_time > 0 && now_time - workers[worker_id].assign_time > TIME_MAX_FOR_PROCESSING){
			// will not push worker_id back to the queue
			files_queue.push(workers[worker_id].file_info);

			// terminate the worker
			workers[worker_id].process_status = PROCESS_DONE;
			MPI_Send(&workers[worker_id].process_status, 1, MPI_INT, worker_id, SEND_RECV_TAG, MPI_COMM_WORLD);

			error_log<<getCtime()<<"\tWorker "<<worker_id<<" has been termimnated since running for too much time"<<endl;
			
			#if HEART_BEAT_STATUS_DEBUG || MANAGER_DEBUG
				printf("%s\tWorker %d has running for too much time, it will be terminated\n", getCtime(), worker_id);
			#endif
			
			sleep(1);
			continue;
		}

		// check the whether worker has finished processing. 
		// if not, push it back to workers_pool;
		// otherwise, handle the report
		// set is_PINGed_heartbeat = true to avoid duplicated Irecv,
		// which cannot function right
        if(workers[worker_id].is_PINGed_jobs == false){
			MPI_Irecv(&workers[worker_id].process_status, 1, MPI_INT, worker_id, SEND_RECV_TAG, MPI_COMM_WORLD, &workers[worker_id].request_jobs);
			workers[worker_id].is_PINGed_jobs = true;
		}

		MPI_Test(&workers[worker_id].request_jobs, &flag, &status);
		
		// worker has not finished, 
		if(flag == 0){
			workers_pool.push(worker_id);
			
			#if HEART_BEAT_STATUS_DEBUG || MANAGER_DEBUG
				printf("%s\tManager notices worker %d is not ready, so push it back to queue\n", getCtime(), worker_id);
			#endif

			sleep(TIME_RELAX);
			continue;
		}
		else{
			printf("%s\tManager notices worker %d is ready\n", getCtime(), worker_id);
			workers[worker_id].is_PINGed_jobs = false;
		}
		

		printf("%s\tManager is dealing with reported processing status %d from worker %d.\n", getCtime(), workers[worker_id].process_status, worker_id);
		if(workers[worker_id].process_status == READ_READY && workers[worker_id].assign_time != 0){
            run_log<<getCtime()<<"\t"<<workers[worker_id].file_info.file_path<<" has been processed by worker "<<worker_id<<endl;
        }

        if(workers[worker_id].process_status == PROCESS_ERROR){
            if(workers[worker_id].file_info.has_touched){
                // if the file has been handled, and error still occurred,
                // we think the file is damaged and add error message to log file
                error_log<<getCtime()<<"\tSomething wrong with "<<workers[worker_id].file_info.file_path<<endl;
				
				#if MANAGER_DEBUG
					printf("%s\t%s might be damaged twice\n", getCtime(), workers[worker_id].file_info.file_path.c_str());
				#endif
            }
            else{
                // try processing this file again
                workers[worker_id].file_info.has_touched = true;
                files_queue.push(workers[worker_id].file_info);
				
				#if MANAGER_DEBUG
					printf("%s\t%s might be damaged once\n", getCtime(), workers[worker_id].file_info.file_path.c_str());
				#endif
            }
        }

		sendFilePath(worker_id);
    }

	// heart beat thread end
	thread_terminate = true;
	for(int i = 0; i < thread_count; i++){
		pthread_join(thread_handles[i], NULL);
	}

	for(int i = 0; i < number_process; i++){
		pthread_mutex_destroy(&mutex_locks[i]);
	}

	free(thread_handles);
	thread_handles = NULL;

    // commit log file
    printf("%s\tAll procedures are done, committing log files\n", getCtime());

	if(files_queue.empty()){
		run_log<<getCtime()<<"\t"<<"All files have been processed"<<endl;
	}
	else{
		run_log<<getCtime()<<"\t"<<"Due to termination of workers, following files have not been processed:"<<endl;
		while(!files_queue.empty()){
			run_log<<"\t\t"<<files_queue.front().file_path<<endl;
			files_queue.pop();
		}
	}

	return 0;
}

/*
   	jobs of workers, including receive file path,
	execute workflow and report results.
*/
int workerRoutine()
{
    MPI_Send(&worker_self.process_status, 1, MPI_INT, 0, SEND_RECV_TAG, MPI_COMM_WORLD);
	int thread_count = 1;
	MPI_Status status;

	// heart beat thread start
	pthread_t* thread_handles = NULL;
	thread_handles = (pthread_t*)malloc(thread_count * sizeof(pthread_t));
	thread_terminate = false;
	for(int i = 0; i < thread_count; i++){
		// pthread_mutex_init(&mutex_locks[i], NULL);
		pthread_create(&thread_handles[i], NULL, heartbeatTestWorker, (void*)(long)i);
    }

	// create tmp dir to process files
	char commd[COMMD_LENGTH];
	memset(tmp_file_dir_for_process, '\0', FILE_DIR_LENGTH);
	sprintf(tmp_file_dir_for_process, "tmp_dir_for_worker_%d", my_rank);
	sprintf(commd, "mkdir -p %s", tmp_file_dir_for_process);
	system(commd);

    while(worker_self.process_status != PROCESS_DONE){
        MPI_Recv(&worker_self.process_status, 1, MPI_INT, 0, SEND_RECV_TAG, MPI_COMM_WORLD, &status);

        if(worker_self.process_status == PROCESS_DONE){
            // do nothing
            worker_self.process_status = PROCESS_EXIT;
            // MPI_Send(&worker_self.process_status, 1, MPI_INT, 0, SEND_RECV_TAG, MPI_COMM_WORLD);
            break;
        }

        // handle file
        if(worker_self.process_status == READY_FOR_FILE){
			char file_path[FILE_DIR_LENGTH];
            MPI_Recv(file_path, FILE_DIR_LENGTH, MPI_INT, 0, SEND_RECV_TAG, MPI_COMM_WORLD, &status);
			worker_self.file_info.file_path = file_path;

			// set up workflow and handle files
			worker_self.process_status = handelFile(file_path);

			// collect result message
            if(worker_self.process_status == FILE_NOT_EXIST || worker_self.process_status == FILE_DAMAGE){
                worker_self.process_status = PROCESS_ERROR;
                //MPI_Send(&worker_self.process_status, 1, MPI_INT, 0, SEND_RECV_TAG, MPI_COMM_WORLD);
            }
            else{
				worker_self.process_status = READ_READY;
                //MPI_Send(&worker_self.process_status, 1, MPI_INT, 0, SEND_RECV_TAG, MPI_COMM_WORLD);
            }

			// report process status to manager
			MPI_Send(&worker_self.process_status, 1, MPI_INT, 0, SEND_RECV_TAG, MPI_COMM_WORLD);
			printf("%s\tWorker %d send process status %d to manager\n", getCtime(), my_rank, worker_self.process_status);
        }
    }

	// heart beat thread end
	printf("%s\tWorker %d starts ending thread\n", getCtime(), my_rank);
	thread_terminate = true;
	for(int i = 0; i < thread_count; i++){
		pthread_join(thread_handles[i], NULL);
		// pthread_mutex_destroy(&mutex_locks[i]);
	}
	free(thread_handles);
	thread_handles = NULL;

    printf("%s\tJobs of worker %d is done\n", getCtime(), my_rank);
	return 0;
}

/*
   	handle files, including get tmp work path ready for process, 
	   set up workflow and clean work path afterward.
	input:		file_path, path of file to process
	output:		report process status
*/
int handelFile(const char* file_path)
{
    int process_status = READ_READY;
	int return_value = 0;
	char commd[COMMD_LENGTH];

	// separate file_name and file_dir from file_path
	string file_path_str = string(file_path);
	string::size_type index_of_name = file_path_str.find_last_of('/') + 1;
    string file_name = file_path_str.substr(index_of_name, file_path_str.length() - index_of_name);
    string file_dir = file_path_str.substr(0, index_of_name - 1);

	// clean tmp work dir
	memset(commd, '\0', COMMD_LENGTH);
	sprintf(commd, "rm %s/%s*", tmp_file_dir_for_process, file_name.c_str());
	printf("%s\tclean tmp work dir %s\n", getCtime(), tmp_file_dir_for_process);
	system(commd);
    
	printf("%s\tWorker %d is handling on %s\n", getCtime(), my_rank, file_name.c_str());

	// set up worker flow
	memset(commd, '\0', COMMD_LENGTH);
	// sprintf(commd, "~/anaconda3/envs/astro-py37/bin/python ../utils/workflow.py --data_dir=%s --file_name=%s --tmp_dir=%s --worker_id=%d", file_dir.c_str(), file_name.c_str(), tmp_file_dir_for_process, my_rank);
	sprintf(commd, "%s %s --data_dir=%s --file_name=%s --tmp_dir=%s --result_dir=%s --model_dir=%s --worker_id=%d", py_interpreter, workflow_organizer, file_dir.c_str(), file_name.c_str(), tmp_file_dir_for_process, dir_to_store_result, dir_of_model, my_rank);
	// printf("process %d start to detect RFI\n", my_rank);
	return_value = system(commd);

	if(return_value == FILE_NOT_EXIST){
		process_status = FILE_NOT_EXIST;
	}

	if(return_value == FILE_DAMAGE){
		process_status = FILE_DAMAGE;
	}

	// memset(commd, '\0', COMMD_LENGTH);
	// sprintf(commd, "mv %s/%s_processed_by_%d.h5 %s/%s.h5", tmp_file_dir_for_process, file_name.c_str(), my_rank, dir_to_store_result, file_name.c_str());  // _processed_by_%d  , my_rank
	// printf("%s\n", commd);
	// system(commd);

	// clean tmp work dir
	memset(commd, '\0', COMMD_LENGTH);
	sprintf(commd, "rm %s/%s*", tmp_file_dir_for_process, file_name.c_str());
	printf("%s\tclean tmp files %s/%s*\n", getCtime(), tmp_file_dir_for_process, file_name.c_str());
	system(commd);

    // if(my_rank < 5){
	// 	sleep(50 + my_rank);
	// }
	// else{
	// 	sleep(60 + my_rank);
	// }

	printf("%s\tWorker %d has handled %s\n", getCtime(), my_rank, file_name.c_str());
	return process_status;
}

/*
   	heart beat thread of manager,
	    communicate with workers to ensure they are running normally
	input:		rank, the thread ID
*/
void* heartbeatTestManager(void* rank)
{
	long my_thread_rank = (long)rank;
	int flag = 0;
    MPI_Status status;
	char greeting[FILE_DIR_LENGTH];

	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	sprintf(greeting, "Greeting to worker %d", my_rank);

	// start heart beat test
	while(!thread_terminate){
		// send greeting to workers every once in a while
		for(int i = 1; i < number_process; i++){
			MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, i, HEART_BEAT_TAG, MPI_COMM_WORLD);
		}
		
		#if THREAD_DEBUG
			printf("%s\tManager send %s\n", getCtime(), greeting);
		#endif

		sleep(1);

		// recv respond from workers, and update last communicate time
		for(int i = 1; i < number_process; i++){

			// MPI_Recv(greeting, FILE_DIR_LENGTH, MPI_CHAR, i, HEART_BEAT_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			// MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, i, HEART_BEAT_TAG, MPI_COMM_WORLD, &request);
			// for(int times = 0; times < TIMES_FOR_RETRY; times++){
			// 	MPI_Test(&request, &flag, &status);
			// 	if(flag == 0){
			// 		if(THREAD_DEBUG)
			// 			printf("Thread %ld of manager not received for %d from %d\n", my_thread_rank, times + 1, i);
			// 		sleep(3);
			// 	}
			// 	else{

			// 		pthread_mutex_lock(&mutex_locks[i]);
			// 		workers[i].last_communicate_time = time(NULL);
			// 		pthread_mutex_unlock(&mutex_locks[i]);

			// 		if(THREAD_DEBUG)
			// 			printf("Thread %ld of worker %d received %s at %ld\n", my_thread_rank, my_rank, greeting, workers[i].last_communicate_time);

			// 		times = TIMES_FOR_RETRY;
			// 		break;
			// 	}
			// }

			// check the whether workers respond,
			// set is_PINGed_heartbeat = true to avoid duplicated Irecv,
			// which cannot function right
			if(workers[i].is_PINGed_heartbeat == false){
				MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, i, HEART_BEAT_TAG, MPI_COMM_WORLD, &workers[i].request_heartbeat);
				workers[i].is_PINGed_heartbeat = true;
			}

			MPI_Test(&workers[i].request_heartbeat, &flag, &status);
			
			// worker does not respond
			if(flag == 0){
				
				#if THREAD_DEBUG
					printf("%s\tManager does not receive greeting from worker %d\n", getCtime(), i);
				#endif

			}
			else{	// respond, update time stamp
				pthread_mutex_lock(&mutex_locks[i]);
				workers[i].last_communicate_time = time(NULL);
				pthread_mutex_unlock(&mutex_locks[i]);

				#if THREAD_DEBUG
					printf("%s\tManager received greeting from worker %d\n", getCtime(), i);
				#endif
				
				workers[i].is_PINGed_heartbeat = false;
			}

			// pthread_mutex_lock(&mutex_locks[i]);
			// workers[i].last_communicate_time = time(NULL);
			// pthread_mutex_unlock(&mutex_locks[i]);

			// printf("thread %d of worker %d received %s at %ld\n", my_thread_rank, my_rank, greeting, workers[i].last_communicate_time);
		}
		sleep(TIME_INTERVAL);
	}

	printf("%s\tFinished thread %ld of worker %d\n", getCtime(), my_thread_rank, my_rank);
	return NULL;
}

/*
   	respond to manager that I am alive
	input:		rank, the thread ID
*/
void* heartbeatTestWorker(void* rank)
{
	long my_thread_rank = (long)rank;
	int flag = 0;
    MPI_Status status;
	char greeting[FILE_DIR_LENGTH];

	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);

	// respond to heart beat test
	while(!thread_terminate){
		// MPI_Recv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, HEART_BEAT_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		// printf("thread %d of worker %d received %s\n", my_thread_rank, my_rank, greeting);

		// MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, HEART_BEAT_TAG, MPI_COMM_WORLD, &request);
		// for(int times = 0; times < TIMES_FOR_RETRY; times++){
		// 	MPI_Test(&request, &flag, &status);
		// 	if(flag == 0){
		// 		if(THREAD_DEBUG)
		// 			printf("%s\tThread %ld of worker %d not received for No.%d from %d\n", getCtime(), my_thread_rank, my_rank, times + 1, 0);
		// 		sleep(3);
		// 	}
		// 	else{
		// 		if(THREAD_DEBUG)
		// 			printf("%s\tThread %ld of worker %d received %s\n", getCtime(), my_thread_rank, my_rank, greeting);

		// 		times = TIMES_FOR_RETRY;
		// 		break;
		// 	}
		// }

		// if(flag != 0){
		// 	// sleep(1);

		// 	sprintf(greeting, "Greeting from thread %ld of worker %d!", my_thread_rank, my_rank);
		// 	MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 0, HEART_BEAT_TAG, MPI_COMM_WORLD);
		// 	if(THREAD_DEBUG)
		// 		printf("send %s\n", greeting);
		// }

		// if(workers[0].is_PINGed_heartbeat == false){
		// 	// MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, HEART_BEAT_TAG, MPI_COMM_WORLD, &workers[my_rank].request_heartbeat);
		// 	// workers[my_rank].is_PINGed_heartbeat = true;
		// 	printf("sending false\n");
		// }
		// MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, HEART_BEAT_TAG, MPI_COMM_WORLD, &request);
		// for(int times = 0; times < TIMES_FOR_RETRY; times++){
		// 	MPI_Test(&request, &flag, &status);
		// 	if(flag == 0){
		// 		if(THREAD_DEBUG)
		// 			printf("%s\tThread %ld of worker %d not received for No.%d from %d\n", getCtime(), my_thread_rank, my_rank, times + 1, 0);
		// 		sleep(3);
		// 	}
		// 	else{
		// 		if(THREAD_DEBUG)
		// 			printf("%s\tThread %ld of worker %d received %s\n", getCtime(), my_thread_rank, my_rank, greeting);

		// 		times = TIMES_FOR_RETRY;
		// 		break;
		// 	}
		// }

		// if(flag != 0){
		// 	// sleep(1);

		// 	sprintf(greeting, "Greeting from thread %ld of worker %d!", my_thread_rank, my_rank);
		// 	MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 0, HEART_BEAT_TAG, MPI_COMM_WORLD);
		// 	if(THREAD_DEBUG)
		// 		printf("send %s\n", greeting);
		// 	// workers[my_rank].is_PINGed_heartbeat = false;
		// }

		// check the whether manager send greeting,
		// set is_PINGed_heartbeat = true to avoid duplicated Irecv,
		// which cannot function right
		if(worker_self.is_PINGed_heartbeat == false){
			MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, HEART_BEAT_TAG, MPI_COMM_WORLD, &worker_self.request_heartbeat);
			worker_self.is_PINGed_heartbeat = true;
		}

		MPI_Test(&worker_self.request_heartbeat, &flag, &status);
		
		// manager has not send greeting
		if(flag == 0){

			#if THREAD_DEBUG
				printf("%s\tWorker %d does not receive greeting from manager\n", getCtime(), my_rank);
			#endif

		}
		else{
			sprintf(greeting, "Greeting from thread %ld of worker %d!", my_thread_rank, my_rank);
			MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 0, HEART_BEAT_TAG, MPI_COMM_WORLD);

			#if THREAD_DEBUG
				printf("%s\tWorker %d received greeting from manager and send back\n", getCtime(), my_rank);
			#endif
			
			worker_self.is_PINGed_heartbeat = false;
		}

		sleep(TIME_INTERVAL);
	}
	printf("%s\tFinished thread %ld of worker %d\n", getCtime(), my_thread_rank, my_rank);
	return NULL;
}

/*
   	parser the argument from the command line
	input:		argc, count of argument
				argv, list of arguments stringS
	output:		assign values to several variable
*/
int getArgument(const int argc, char *argv[])
{
	// if(argc == 2 && strcmp(argv[1], "-h") == 0){
	// 	printf("Usage: \n\t./pipeline \n\t-i <Input file dir> \n\t-s <node name where stored file> \n\t-n <process number> \n\t-o <dir to store results>\n");
	// 	return 0;
	// }

	// int opt = 0;
	// while((opt = getopt(argc, argv, "i:n:o:")) != -1) {
    //     switch(opt) {
    //         case 'i':
    //             strncpy(dir_to_process, optarg, FILE_DIR_LENGTH);
    //             break;
    //         case 'o':
    //             strncpy(dir_to_store_result, optarg, FILE_DIR_LENGTH);
    //             break;
    //         case 's':
    //             strncpy(stored_file_node, optarg, FILE_DIR_LENGTH);
    //             break;
    //         case 'n':
    //             files_num_to_process = atoi(optarg);
    //             break;
	// 		default:
	// 			printf("unknow input arg %c", opt);
	// 			return 1;
    //     }
    // }

	// if(my_rank == 0){
	// 	printf("dir_to_process:%s\ndir_to_store_result:%s\nstored_file_node:%s\nprocess_files_num:%d\n", dir_to_process, dir_to_store_result, stored_file_node, files_num_to_process);
	// }

	// return 0;

	int opt;
    int option_index = 0;
    char options[] = "a:b:c:d:e:f:";
	
    static struct option long_options[] =
    {  
        {"dir_to_process",			required_argument, NULL, 'a'},
        {"dir_to_store_result",		required_argument, NULL, 'b'},
        {"stored_file_node",		required_argument, NULL, 'c'},
        {"files_num_to_process",	required_argument, NULL, 'd'},
		{"workflow_organizer",		required_argument, NULL, 'e'},
		{"py_interpreter",			required_argument, NULL, 'f'},
		{"dir_of_model",			required_argument, NULL, 'g'},
    }; 
	
	// ./getopt --workflow_organizer worker.py
    while((opt = getopt_long_only(argc, argv, options, long_options, &option_index))!= -1)
    {  
        // printf("opt = %c\t\t", opt);
        // printf("optarg = %s\t\t",optarg);
        // printf("optind = %d\t\t",optind);
        // printf("argv[optind] =%s\t\t", argv[optind]);
        // printf("option_index = %d\n",option_index);
		
		switch(opt) {
            case 'a':
                strncpy(dir_to_process, optarg, FILE_DIR_LENGTH);
                break;
            case 'b':
                strncpy(dir_to_store_result, optarg, FILE_DIR_LENGTH);
                break;
            case 'c':
                strncpy(stored_file_node, optarg, FILE_DIR_LENGTH);
                break;
            case 'd':
                files_num_to_process = atoi(optarg);
                break;
			case 'e':
                strncpy(workflow_organizer, optarg, FILE_DIR_LENGTH);
                break;
			case 'f':
                strncpy(py_interpreter, optarg, FILE_DIR_LENGTH);
                break;
			case 'g':
                strncpy(dir_of_model, optarg, FILE_DIR_LENGTH);
                break;
			default:
				printf("unknow input arg %s", argv[optind]);
				return 1;
        }
    }
	
	if(my_rank == 0)
	printf("dir_to_process:\t%s\ndir_to_store_result:\t%s\nstored_file_node:\t%s\nprocess_files_num:\t%d\nworkflow_organizer:\t%s\npy_interpreter:\t%s\ndir_of_model:\t%s\n", dir_to_process, dir_to_store_result, stored_file_node, files_num_to_process, workflow_organizer, py_interpreter, dir_of_model);

	return 0;
}

int main(int argc, char *argv[])
{
	int provided, name_len;
	char node_name[FILE_DIR_LENGTH];				// the node that the process is running at
	MPI_Init_thread(&argc,&argv,MPI_THREAD_MULTIPLE, &provided); // initializes the MPI environment
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 		// gets the current process ID
	MPI_Comm_size(MPI_COMM_WORLD, &number_process); // gets the total number of processes
	MPI_Get_processor_name(node_name, &name_len);

	getArgument(argc, argv);

	if(my_rank == 0){
		printf("Manager is on %s\n", node_name);
	    managerRoutine();
	}
	else{
		printf("Worker %d of %d is on %s\n", my_rank, number_process - 1, node_name);
	    workerRoutine();
	}

	//can MPI_BLOCK();
	MPI_Finalize();
	// int end_time = time(NULL);
	// cout<<end_time - start_time<<endl;
	return 0;
}
