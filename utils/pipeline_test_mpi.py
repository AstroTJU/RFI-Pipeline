# -*- coding:utf-8 -*-
"""  
#====#====#====#====
# Project Name:     RFI-Net
# File Name:        pipeline
# Date:             2020/4/27 12:10 
# Using IDE:        PyCharm 
# From HomePage:    https://github.com/DuFanXin/RFI-Net
# Note:             
# Author:           DuFanXin 
# BlogPage:         http://blog.csdn.net/qq_30239975  
# E-mail:           18672969179@163.com
# Copyright (c) 2020, All Rights Reserved.
#====#====#====#==== 
"""
from mpi4py import MPI
import queue
import sys
sys.path.append("..")
from utils import process_files
import time


""" MPI Initialization """
comm = MPI.COMM_WORLD
comm_rank = comm.Get_rank()
comm_size = comm.Get_size()
node_name = MPI.Get_processor_name()    # get the name of the node

print("process %d is running on %s" % (comm_rank, node_name))

if()

# work_flow()

