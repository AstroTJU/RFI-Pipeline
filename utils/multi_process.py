# -*- coding:utf-8 -*-
"""  
#====#====#====#====
# Project Name:     RFI-Net
# File Name:        multi_process
# Date:             2020/2/17 19:12 
# Using IDE:        PyCharm 
# From HomePage:    https://github.com/DuFanXin/RFI-Net
# Note:             
# Author:           DuFanXin 
# BlogPage:         http://blog.csdn.net/qq_30239975  
# E-mail:           18672969179@163.com
# Copyright (c) 2020, All Rights Reserved.
#====#====#====#==== 
"""
import multiprocessing
import sys
sys.path.append("..")
import time
# from utils import process_files
from model import rfi_net
import numpy as np
from astropy.io import fits
import h5py
import os



POLAR = 2
ROW = 65536  # channel
COLUMN = 2048  # time
SEGMENT_ROW = 256
SEGMENT_COLUMN = 128
SEGMENTS_NUMBER = (COLUMN // SEGMENT_COLUMN) * (ROW // SEGMENT_ROW)
BAIS = 0.000001


class MultiProcess(multiprocessing.Process):
    def __init__(self, process_ID):
        multiprocessing.Process.__init__(self)
        self.process_ID = process_ID

    def run(self):
        # n = 5
        # while n > 0:
        #     print("%s the time is %s" % (self.interval, time.ctime()))
        #     time.sleep(0.5)
        #     n -= 1
        # read_and_convert_fits_to_hdf5(
        #     fits_file_name="./Dec+0551_drifting-M19_W_0019_%d.fits" % self.process_ID,
        #     hdf5_file_name="./convert_Dec+0551_drifting-M19_W_0019_%d.h5" % self.process_ID
        # )
        net = rfi_net.RFI_Net()
        net.test(
            test_file_path="../data_set/real_image/test_set.h5", 
            model_file_path="../data_set/saved_models/rfi_net_real_data",
            test_result_path="../data_set/test_result_rfi_net_%d" % self.process_ID
        )


def read_and_convert_fits_to_hdf5(fits_file_name=None, hdf5_file_name=None):
    """
    Function:
            read a fits file and extract data into a hdf5 file
    :param hdf5_file_name:
    :param fits_file_name:
    :return:
    """
    fast = fits.open(fits_file_name, lazy_load_hdus=True)
    # fast.info()
    # print(fast[1].data[1].field(20).shape)
    # print(fast[1].data.names)  # show every comment of each item

    # access item DATA
    data_shape = fast[1].data[1].field('DATA').shape
    data_row = data_shape[0]  # channel
    data_polar = data_shape[1]  # polarization
    data_column = fast[1].data.shape[0]  # time
    print("%s %d %d %d" % (fits_file_name, data_polar, data_row, data_column))

    # [polar, channel, time]
    convert_data = np.ones(shape=(data_polar, data_row, data_column), dtype=np.float64)
    for col in range(data_column):
        for polar in range(data_polar):
            convert_data[polar, :, col] = fast[1].data[col].field('DATA')[:, polar]

    with h5py.File(os.path.join("", hdf5_file_name), 'w') as fast_h5:
        for i in range(POLAR):
            # transpose [channel, time] to [time, channel]
            fast_h5['polarization_%d' % i] = convert_data[i].T[::-1, :]
            print("convert polar %d of %s" % (i, fits_file_name))

    print("Done converting, data is stored in hdf5 file %s" % hdf5_file_name)
    return 0


def main():
    p1 = MultiProcess(1)
    p2 = MultiProcess(2)
    p3 = MultiProcess(3)
    p4 = MultiProcess(4)
    p1.start()
    p2.start()
    p3.start()
    p4.start()
    print("end of multi process")


if __name__ == '__main__':
    main()
