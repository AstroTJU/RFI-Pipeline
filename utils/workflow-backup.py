# -*- coding:utf-8 -*-
"""  
#====#====#====#====
# Project Name:     RFI-Net
# File Name:        test_whole_pipeline
# Date:             2020/3/9 13:52 
# Using IDE:        PyCharm 
# From HomePage:    https://github.com/DuFanXin/RFI-Net
# Note:             
# Author:           DuFanXin 
# BlogPage:         http://blog.csdn.net/qq_30239975  
# E-mail:           18672969179@163.com
# Copyright (c) 2020, All Rights Reserved.
#====#====#====#==== 
"""
import unittest
# from utils import find_files
# from ddt import ddt, data, unpack
# from parameterized import parameterized
import sys
import argparse
import time

sys.path.append("..")
from utils import process_files


parser = argparse.ArgumentParser()


class TestProcessFiles(unittest.TestCase):
    """
    This class is to test the functions of process files
    """
    @classmethod
    def setUpClass(cls):
        print("#====" * 5 + " start test process file" + "====#" * 5)

    @classmethod
    def tearDownClass(cls):
        print("#====" * 5 + " end of test process file" + "====#" * 5)

    # @unittest.skip("Not to run this case now.")
    def test_read_and_convert_fits_to_hdf5(self, fits_file_name, hdf5_file_name):
        start_time = time.time()
        process_files.read_and_convert_fits_to_hdf5(
            fits_file_name=fits_file_name,
            hdf5_file_name=hdf5_file_name)
        print("done read and convert using %.2f" % (time.time() - start_time))
        self.assertEqual(0, 0)

    # @unittest.skip("Not to run this case now.")
    def test_segment_hdf5_to_required_size(self, hdf5_file_name, after_segment_file_name):
        start_time = time.time()
        process_files.segment_hdf5_to_required_size(
            hdf5_file_name=hdf5_file_name,
            after_segment_file_name=after_segment_file_name)
        print("done segmenting using %.2f" % (time.time() - start_time))
        self.assertEqual(0, 0)

    # @unittest.skip("Not to run this case now.")
    def test_rfi_net_predicted(self, predict_batch_size, need_predict_file_path, predict_result_path, model_file_path, start_index, data_num):
        from model import rfi_net_predict_separately
        start_time = time.time()
        net = rfi_net_predict_separately.RFI_Net()
        net.predict(predict_batch_size=predict_batch_size,
                    need_predict_file_path=need_predict_file_path,
                    predict_result_path=predict_result_path,
                    model_file_path=model_file_path,
                    start_index=start_index,
                    data_num=data_num)
        print("done predicting using %.2f" % (time.time() - start_time))
        self.assertEqual(0, 0)

    # @unittest.skip("Not to run this case now.")
    def test_restore_segment_hdf5_files(self, hdf5_file_name, restored_file_name):
        start_time = time.time()
        process_files.restore_segment_hdf5_files(
            hdf5_file_name=hdf5_file_name,
            restored_file_name=restored_file_name)
        print("done restore using %.2f" % (time.time() - start_time))
        self.assertEqual(0, 0)

def test_without_rfi_net():
    test_pipeline = TestProcessFiles()
    print("start process files")
    start_time = time.time()
    args = parser.parse_args()
    data_dir = args.data_dir
    file_name = args.file_name
    tmp_dir = args.tmp_dir
    process_id = args.process_id
    # "../data_set/Dec+0551_drifting-M19_W_0019.fits"
    test_pipeline.test_read_and_convert_fits_to_hdf5(
        fits_file_name="%s/%s" % (data_dir, file_name),
        hdf5_file_name="%s/%s_converted_by_process_%d.h5" % (tmp_dir, file_name, process_id))

    test_pipeline.test_segment_hdf5_to_required_size(
        hdf5_file_name="%s/%s_converted_by_process_%d.h5" % (tmp_dir, file_name, process_id),
        after_segment_file_name="%s/%s_segmented_by_process_%d.h5" % (tmp_dir, file_name, process_id))

    
    # import tensorflow as tf
    # print(tf)

    test_pipeline.test_restore_segment_hdf5_files(
        hdf5_file_name="%s/%s_segmented_by_process_%d.h5" % (tmp_dir, file_name, process_id),
        restored_file_name="%s/%s_restored_by_process_%d.h5" % (tmp_dir, file_name, process_id))

    print("end process files, using %.2fs" % (time.time() - start_time))

def test_with_rfi_net():
    test_pipeline = TestProcessFiles()
    print("start process files")
    start_time = time.time()
    args = parser.parse_args()
    data_dir = args.data_dir
    file_name = args.file_name
    tmp_dir = args.tmp_dir
    process_id = args.process_id
    result_dir = args.result_dir
    # "../data_set/Dec+0551_drifting-M19_W_0019.fits"
    test_pipeline.test_read_and_convert_fits_to_hdf5(
        fits_file_name="%s/%s" % (data_dir, file_name),
        hdf5_file_name="%s/%s_converted_by_process_%d.h5" % (tmp_dir, file_name, process_id))

    test_pipeline.test_segment_hdf5_to_required_size(
        hdf5_file_name="%s/%s_converted_by_process_%d.h5" % (tmp_dir, file_name, process_id),
        after_segment_file_name="%s/%s_segmented_by_process_%d.h5" % (tmp_dir, file_name, process_id))

    for i in range(4):
        test_pipeline.test_rfi_net_predicted(
            predict_batch_size=64,
            need_predict_file_path="%s/%s_segmented_by_process_%d.h5" % (tmp_dir, file_name, process_id),
            predict_result_path="%s/%s_predicted_by_process_%d.h5" % (tmp_dir, file_name, process_id),
            model_file_path="../data_set/saved_models",
            start_index=2048*i,
            data_num=2048)
    
    test_pipeline.test_restore_segment_hdf5_files(
        hdf5_file_name="%s/%s_predicted_by_process_%d.h5" % (tmp_dir, file_name, process_id),
        restored_file_name="%s/%s_processed_by_%d.h5" % (result_dir, file_name, process_id))   
        # restored_file_name="%s/%s_restored_by_process_%d.h5" % (tmp_dir, file_name, process_id))

    print("end process files, using %.2fs" % (time.time() - start_time))

def main():
    # test_suite = unittest.TestSuite()
    # # tests = [TestFindFiles("test_walk_files")]
    # # test_suite.addTests(tests)
    # test_suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestProcessFiles))
    # # test_suite.addTest(TestFindFiles("test_walk_files"))
    #
    # with open('Unittest_whole_pipeline_Report.txt', 'w') as f:
    #     runner = unittest.TextTestRunner(stream=f, verbosity=2)
    #     runner.run(test_suite)
    test_with_rfi_net()
    


if __name__ == '__main__':

    # data path
    parser.add_argument('--data_dir', type=str, help='dir for input data')
    
    # file name
    parser.add_argument('--file_name', type=str, help='file`s name')
    
    # tmp file dir
    parser.add_argument('--tmp_dir', type=str, help='dir for tmp processed file')

    # result saved into
    parser.add_argument('--result_dir', type=str, help='output result path')

    # process ID
    parser.add_argument('--process_id', type=int, help='process ID')

    # IP address
    parser.add_argument('--ip_address', type=str, help='IP address')

    main()
