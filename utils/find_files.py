# -*- coding:utf-8 -*-
"""  
#====#====#====#====
# Project Name:     RFI-Net
# File Name:        find_files
# Date:             2020/1/14 21:28 
# Using IDE:        PyCharm 
# From HomePage:    https://github.com/DuFanXin/RFI-Net
# Note:             
# Author:           DuFanXin 
# BlogPage:         http://blog.csdn.net/qq_30239975  
# E-mail:           18672969179@163.com
# Copyright (c) 2020, All Rights Reserved.
#====#====#====#==== 
"""
import os
from os.path import join
# import glob
import argparse
FLAGS = None


def convert_fits_to_hdf5(file_list=None):
    """
    Function:
        convert fits files into hdf5 file. each polarization is

    Parameter:
        file_list: a list, in which wanted files are given

    Return:
        a number of hdf5 files stored in disk
    """
    return 0


def walk_files(file_path=None, file_types=None):
    """
        Function:
                find all names of files of which the type is in file_types

        Parameters:
                file_path: a string, the top directory that you want to search
                file_types: a tuple, the type you want

        Returns: (files_number, files_list)
                files_number: int, the total number of files you want from top directory
                files_list: a list, the final list of files you want to find

        Example:
                from utils import find_files
                files_number, files_list = find_files.walk_files(file_path="./")
    """
    if file_path is None:
        file_path = FLAGS.data_dir
    if file_types is None:
        file_types = [".fits"]

    files_number, files_list = 0, []
    for dir_path, dir_names, file_names in os.walk(file_path):
        files_num_in_current_folder = 0
        for file_name in file_names:
            # print(join(dir_path, file_name))
            # check whether the type of file is "fits"
            if os.path.splitext(file_name)[1] in file_types:
                files_list.append(join(dir_path, file_name))
                print(join(dir_path, file_name))
                files_num_in_current_folder += 1

        print("%d files in %s" % (files_num_in_current_folder, dir_path))
        files_number += files_num_in_current_folder

    print('Total %d files' % len(files_list))

    return files_number, files_list


def main():
    # print(FLAGS.data_dir)
    walk_files()
    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # FITS data path
    parser.add_argument(
        '--data_dir', type=str, default="./",
        help='Directory of FITS files')

    FLAGS, _ = parser.parse_known_args()

    main()
