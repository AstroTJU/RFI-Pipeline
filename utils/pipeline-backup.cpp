#include <mpi.h>
#include <unistd.h>
#include <bits/stdc++.h>
using namespace std;

#define READ_READY 		0
#define READY_FOR_FILE	1
#define PROCESS_DONE 	-1
#define PROCESS_ERROR	-2
#define PROCESS_EXIT	-3
#define FILE_DIR_LENGTH 100
#define COMMD_LENGTH 1024
// #define path = dir + file name

queue<string> file_names_queue;
queue<int> process_queue;
vector<string> process_to_file;
int tag = 0; // communication marks
MPI_Status status;
char ip[20] = "127.0.0.1";
char stored_file_node[20] = "TJU-FAST-RFI";
int my_rank; // current process ID
int np; // total number of processes
int name_len;
char node_name[FILE_DIR_LENGTH];	// the node that process is running at
char dir_to_process[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set"; // dir in which there are files to be processed
char dir_to_store_result[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set/processed_results";
ofstream error_log("error.log");
ofstream run_log("run.log");
int process_num = 0;

int send_file_name(int process_id)
{
	int process_status;
	if(!file_names_queue.empty()){
		process_status = READY_FOR_FILE;
		MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);

		char file_name[FILE_DIR_LENGTH];
		strncpy(file_name, file_names_queue.front().c_str(), FILE_DIR_LENGTH);
		printf("sending file name %s\n", file_name);
		MPI_Send(file_name, FILE_DIR_LENGTH, MPI_CHAR, process_id, tag, MPI_COMM_WORLD);

		process_to_file[process_id] = file_names_queue.front();
		file_names_queue.pop();
		
		process_queue.push(process_id);
	}
	else{
		process_status = PROCESS_DONE;
		MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
		run_log<<"process "<<process_id<<" is done working"<<endl;
	}

	return 0;
}

int manager()
{
	/*
	TODO: find files in directories
	*/
	// make directory for 
	// char commd[COMMD_LENGTH];
	// sprintf(commd, "ssh %s \"mkdir -p %s\" ", stored_file_node, dir_to_store_result);
	// system(commd);
	
	// FILE *fp;
	// char file_name[FILE_DIR_LENGTH];
	// int total_file_num = 0;
	// fp = popen("cd /home/YZC/projects/MPI/data_set && for entry in `ls Dec*`; do echo $entry\ndone", "r");
	// while(fgets(file_name, sizeof(file_name), fp) != NULL){
			// printf("%d %s", total_file_num++, file_name);
			// file_names_queue.push(file_name);
	// }
	// pclose(fp);	
	
	for(int i = 0; i < process_num; i++){
		// char file_dir[FILE_DIR_LENGTH];
		// sprintf(file_dir, "Dec+0551_drifting-M19_W_00%d.fits", 18 + i);
		// string file_dir_s(file_dir);
		// file_names_queue.push(file_dir_s);
		file_names_queue.push("Dec+0551_drifting-M19_W_0018.fits");
	}


    for(int i = 1; i < np; i++){
        process_queue.push(i);
    }

    while(!process_queue.empty()){
        int process_id = process_queue.front();
        int process_status = 0;
        process_queue.pop();
        MPI_Recv(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD, &status);

        if(process_status == READ_READY){
            send_file_name(process_id);
        }

        if(process_status == PROCESS_ERROR){
            // add error message to log file
            error_log<<"something wrong with "<<process_to_file[process_id]<<endl;
            send_file_name(process_id);
        }
    }
    // commit log file
    printf("all procedures done, committing log files\n");
}

int processor()
{
    int process_status = READ_READY;
    MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
    bool something_wrong = false;
	
	// make directory for 
	char commd[COMMD_LENGTH];
	sprintf(commd, "mkdir -p %s", dir_to_store_result);
	system(commd);

    while(process_status != PROCESS_DONE){
        MPI_Recv(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);

        if(process_status == PROCESS_DONE){
            // do nothing
            process_status = PROCESS_EXIT;
            // MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            break;
        }

        /* handle file */
        if(process_status == READY_FOR_FILE){
            char file_name[FILE_DIR_LENGTH];
            MPI_Recv(file_name, FILE_DIR_LENGTH, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);

            printf("Process %d is handling on %s\n", my_rank, file_name);
			
			system("mkdir -p tmp_dir_for_process");
			
			// string tmp_file_dir_for_process = string("tmp_dir_for_process/") + string(file_name);
			string tmp_file_dir_for_process = "tmp_dir_for_process";

            // char commd[COMMD_LENGTH];

            // sprintf(commd, "scp %s:%s/%s %s", stored_file_node, dir_to_process, file_name, tmp_file_path_for_process.c_str());
			// sprintf(commd, "cp %s/%s %s", dir_to_process, file_name, tmp_file_path_for_process.c_str());
            // printf("%s\n", commd);
            // system(commd);

            memset(commd, '\0', COMMD_LENGTH);
			//  sprintf(commd, "#!/bin/bash\n\nsource ~/anaconda3/etc/profile.d/conda.sh\nconda activate py37-tf113\ncd ../unit_test\npython test_whole_pipeline_except_RFI-Net.py --data_dir=../utils/%s --process_id=%d", tmp_file_path_for_process.c_str(), my_rank);
            sprintf(commd, "cd ../unit_test && ~/anaconda3/envs/py37-tf113/bin/python test_whole_pipeline_except_RFI-Net.py --data_dir=%s --file_name=%s --tmp_dir=../utils/%s --result_dir=%s --process_id=%d", dir_to_process, file_name, tmp_file_dir_for_process.c_str(), dir_to_store_result, my_rank);
            // printf("%s\n", commd);
			//envs/py37-tf113/
			printf("process %d start to detect RFI\n", my_rank);
            // ofstream cout_file("process_file.sh");
            // cout_file<<commd<<endl;
            // system("bash ./process_file.sh");
			system(commd);

            // memset(commd, '\0', COMMD_LENGTH);
            // sprintf(commd, "scp %s_restored_by_process_%d.h5 %s:%s/%s_processed_by_%d.h5", tmp_file_path_for_process.c_str(), my_rank, stored_file_node, dir_to_store_result, file_name, my_rank);
			// sprintf(commd, "mv %s/%s_restored_by_process_%d.h5 %s/%s_processed_by_%d.h5", tmp_file_dir_for_process.c_str(), file_name, my_rank, dir_to_store_result, file_name, my_rank);
            // printf("%s\n", commd);
            // system(commd);

            memset(commd, '\0', COMMD_LENGTH);
            sprintf(commd, "rm %s/%s*", tmp_file_dir_for_process.c_str(), file_name);
            printf("%s\n", commd);
            system(commd);

            printf("Process %d has handled %s\n", my_rank, file_name);


            if(something_wrong){
                process_status = PROCESS_ERROR;
                MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            }
            else{
                process_status = READ_READY;
                MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            }
        }
    }
    printf("Work of Process %d is done\n", my_rank);
}

int process_routine()
{
	process_to_file.resize(np);

	if(my_rank == 0){
	    manager();
	}
	else{
	    processor();
	}

	return 0;
}

int main(int argc, char *argv[])
{


	MPI_Init(&argc, &argv); // initializes the MPI environment
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); // gets the current process ID
	MPI_Comm_size(MPI_COMM_WORLD, &np); // gets the total number of processes
	MPI_Get_processor_name(node_name, &name_len);
	
	printf("Process %d of %d is on %s\n", my_rank, np, node_name);

	if(argc == 2 && strcmp(argv[1], "-h") == 0){
		printf("Usage: \n\t./pipeline \n\t-i <Input file dir> \n\t-s <node name where stored file> \n\t-n <process number> \n\t-o <dir to store results>\n");
		return 0;
	}
	
	int opt = 0;
	while((opt = getopt(argc, argv, "i:n:o:")) != -1) {
        switch(opt) {
            case 'i':
                strncpy(dir_to_process, optarg, FILE_DIR_LENGTH);
                break;
            case 'o':
                strncpy(dir_to_store_result, optarg, FILE_DIR_LENGTH);
                break;
            case 's':
                strncpy(stored_file_node, optarg, FILE_DIR_LENGTH);
                break;
            case 'n':
                process_num = atoi(optarg);
                break;
			default:
				printf("unknow input arg %c", opt);
				return 1;
        }
    }
	
	printf("dir_to_process:%s\ndir_to_store_result:%s\nstored_file_node:%s\nprocess_files_num:%d\n", dir_to_process, dir_to_store_result, stored_file_node, process_num);
	
	process_routine();

	//can MPI_BLOCK();
	MPI_Finalize();
	return 0;
}

// yhrun -N 2 -n 4 -p th_mt1 ./mpi-2
// yhrun -N 2 -p th_mt1  mpirun -n 4 -f ./cpi_config ./mpi-2