# -*- coding:utf-8 -*-
"""  
#====#====#====#====
# Project Name:     RFI-Net
# File Name:        pipeline
# Date:             2020/4/27 12:10 
# Using IDE:        PyCharm 
# From HomePage:    https://github.com/DuFanXin/RFI-Net
# Note:             
# Author:           DuFanXin 
# BlogPage:         http://blog.csdn.net/qq_30239975  
# E-mail:           18672969179@163.com
# Copyright (c) 2020, All Rights Reserved.
#====#====#====#==== 
"""
from mpi4py import MPI
import queue
import subprocess
import argparse
import sys
sys.path.append("..")
from utils import process_files
from model import rfi_net
import time

""" Signals """
READ_READY = 0
READY_FOR_FILE = 1
PROCESS_DONE = -1
PROCESS_ERROR = -2
PROCESS_EXIT = -3
FILE_NAME_LENGTH = 30
# path = dir + file name

""" Global Value """
file_list = []

file_being_processed = []
file_index = 0
process_queue = queue.Queue()
stored_file_node = "TJU-FAST-RFI"
dir_to_process = "/home/YZC/projects/MPI/data_set"
dir_to_store_result = "/home/YZC/projects/MPI/data_set/processed_results"
parser = argparse.ArgumentParser()
file_num = 0

""" MPI Initialization """
comm = MPI.COMM_WORLD
my_rank = comm.Get_rank()
comm_size = comm.Get_size()
node_name = MPI.Get_processor_name()    # get the name of the node


def test_read_and_convert_fits_to_hdf5(fits_file_name, hdf5_file_name):
    start_time = time.time()
    process_files.read_and_convert_fits_to_hdf5(
        fits_file_name=fits_file_name,
        hdf5_file_name=hdf5_file_name)
    print("done read and convert using %.2f" % (time.time() - start_time))


def test_segment_hdf5_to_required_size(hdf5_file_name, after_segment_file_name):
    start_time = time.time()
    process_files.segment_hdf5_to_required_size(
        hdf5_file_name=hdf5_file_name,
        after_segment_file_name=after_segment_file_name)
    print("done segmenting using %.2f" % (time.time() - start_time))


def test_rfi_net_predicted(predict_batch_size, need_predict_file_path, predict_result_path, model_file_path):
    start_time = time.time()
    net = rfi_net.RFI_Net()
    net.predict(predict_batch_size=predict_batch_size,
                need_predict_file_path=need_predict_file_path,
                predict_result_path=predict_result_path,
                model_file_path=model_file_path)
    print("done predicting using %.2f" % (time.time() - start_time))


def test_restore_segment_hdf5_files(hdf5_file_name, restored_file_name):
    start_time = time.time()
    process_files.restore_segment_hdf5_files(
        hdf5_file_name=hdf5_file_name,
        restored_file_name=restored_file_name)
    print("done restore using %.2f" % (time.time() - start_time))


def process_file(file_name):
    test_read_and_convert_fits_to_hdf5(
        fits_file_name=file_name,
        hdf5_file_name="%s_converted_by_process_%d.h5" % (file_name, my_rank))

    test_segment_hdf5_to_required_size(
        hdf5_file_name="%s_converted_by_process_%d.h5" % (file_name, my_rank),
        after_segment_file_name="%s_segmented_by_process_%d.h5" % (file_name, my_rank))

    test_rfi_net_predicted(
        predict_batch_size=64,
        need_predict_file_path="%s_segmented_by_process_%d.h5" % (file_name, my_rank),
        predict_result_path="%s_predicted_by_process_%d.h5" % (file_name, my_rank),
        model_file_path="../data_set/saved_models")

    test_restore_segment_hdf5_files(
        hdf5_file_name="%s_predicted_by_process_%d.h5" % (file_name, my_rank),
        restored_file_name="%s_restored_by_process_%d.h5" % (file_name, my_rank))


def send_file_name(process_id):
    global file_index

    if file_index < len(file_list):
        comm.send(READY_FOR_FILE, dest=process_id)
        comm.send(file_list[file_index], dest=process_id)
        # file_being_processed[process_id] = file_list[file_index]
        file_index += 1
        process_queue.put(process_id)
    else:
        comm.send(PROCESS_DONE, dest=process_id)


def manager():
    for i in range(file_num):
        file_list.append("Dec+0551_drifting-M19_W_0018.fits")

    for i in range(1, comm_size):
        process_queue.put(i)

    while not process_queue.empty():
        process_id = process_queue.get()

        receive_message = comm.recv(source=process_id)

        if receive_message == READ_READY:
            send_file_name(process_id)

        if receive_message == PROCESS_ERROR:
            # TODO: add error message to log file
            send_file_name(process_id)


def processor():
    comm.send(READ_READY, dest=0)
    something_wrong = False
    while 1:
        receive_message = comm.recv(source=0)

        if receive_message == PROCESS_DONE:
            break

        if receive_message == READY_FOR_FILE:
            file_name = comm.recv(source=0)

            print("Process %d is handling %s" % (my_rank, file_name))

            # test_read_and_convert_fits_to_hdf5(
            #     fits_file_name=file_name, hdf5_file_name="%s_convert_by_%d" % (file_name, my_rank))
            #
            # test_segment_hdf5_to_required_size(
            #     hdf5_file_name="%s_convert_by_%d" % (file_name, my_rank),
            #     after_segment_file_name="%s_segment_by_%d" % (file_name, my_rank))
            #
            # # detect RFIs,    real 2m14.194s     user 5m7.074s   sys 0m36.602s
            #
            # test_restore_segment_hdf5_files(
            #     hdf5_file_name="%s_segment_by_%d" % (file_name, my_rank),
            #     restored_file_name="%s_restored_by_%d" % (file_name, my_rank))

            tmp_file_path_for_process = "tmp_dir_for_process/" + file_name

            command = "scp %s:%s/%s %s" % (stored_file_node, dir_to_process, file_name, tmp_file_path_for_process)
            print(command)
            status, result = subprocess.getstatusoutput(command)

            print("detecting RFI")
            process_file(tmp_file_path_for_process)

            command = "scp %s_restored_by_process_%d.h5 %s:%s/%s_processed_by_%d.h5" % (tmp_file_path_for_process, my_rank, stored_file_node, dir_to_store_result, file_name, my_rank)
            print(command)
            status, result = subprocess.getstatusoutput(command)

            command = "rm %s*" % tmp_file_path_for_process
            print(command)
            status, result = subprocess.getstatusoutput(command)

            print("Process %d is done dealing with %s" % (my_rank, file_name))

        if something_wrong is True:
            comm.send(PROCESS_ERROR, dest=0)
        else:
            comm.send(READ_READY, dest=0)
    
    print("Work of Process %d on %s is done" % (my_rank, node_name))


def work_flow():
    print("Process %d of %d is on %s" % (my_rank, comm_size, MPI.Get_processor_name()))

    if my_rank == 0:
        manager()
    else:
        processor()

    print("whole pipeline is done working")


def test():
    if my_rank == 0:
        for i in range(1, 4):
            str = comm.recv(source=i)
            print(str)
    else:
        str = "greet from %d" % my_rank
        comm.send(str, dest=0)


def main():
    global dir_to_process, dir_to_store_result, stored_file_node, file_num, file_list
    args = parser.parse_args()
    dir_to_process = args.dir_to_process
    dir_to_store_result = args.dir_to_store_result
    stored_file_node = args.stored_file_node
    file_num = args.file_num
    # test()

    work_flow()


if __name__ == '__main__':
    # data path
    parser.add_argument('--dir_to_process', type=str, default="/home/YZC/projects/MPI/data_set", help='path for input data')

    # result saved into path
    parser.add_argument('--dir_to_store_result', type=str, default="/home/YZC/projects/MPI/data_set/processed_results", help='output result path')

    # stored file node
    parser.add_argument('--stored_file_node', type=str, default="TJU-FAST-RFI", help='node in which files are stored')

    # file num
    parser.add_argument('--file_num', type=int, default=0, help='num of files to be process')

    main()
