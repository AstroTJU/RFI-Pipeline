# -*- coding:utf-8 -*-
"""  
#====#====#====#====
# Project Name:     RFI-Net
# File Name:        process_files
# Date:             2020/2/9 18:12 
# Using IDE:        PyCharm 
# From HomePage:    https://github.com/DuFanXin/RFI-Net
# Note:             
# Author:           DuFanXin 
# BlogPage:         http://blog.csdn.net/qq_30239975  
# E-mail:           18672969179@163.com
# Copyright (c) 2020, All Rights Reserved.
#====#====#====#==== 
"""
import numpy as np
from astropy.io import fits
import h5py
import os

POLAR = 2
ROW = 2048  # time
COLUMN = 65536  # channel
SEGMENT_ROW = 256
SEGMENT_COLUMN = 128
SEGMENTS_NUMBER = (COLUMN // SEGMENT_COLUMN) * (ROW // SEGMENT_ROW)
BAIS = 0.000001


def read_and_convert_fits_to_hdf5(fits_file_name=None, hdf5_file_name=None):
    """
    Function:
            read a fits file and extract data into a hdf5 file
    :param hdf5_file_name:
    :param fits_file_name:
    :return:
    """
    fast = fits.open(fits_file_name, lazy_load_hdus=True)
    # fast.info()
    # print(fast[1].data[1].field(20).shape)
    # print(fast[1].data.names)  # show every comment of each item

    # access item DATA
    data_shape = fast[1].data[1].field('DATA').shape
    data_row = data_shape[0]  # channel
    data_polar = data_shape[1]  # polarization
    data_column = fast[1].data.shape[0]  # time
    print("start to convert %s %d %d %d" % (fits_file_name, data_polar, data_row, data_column))

    # [polar, channel, time]
    convert_data = np.ones(shape=(data_polar, data_row, data_column), dtype=np.float32)
    for col in range(data_column):
        for polar in range(data_polar):
            convert_data[polar, :, col] = fast[1].data[col].field('DATA')[:, polar]

    with h5py.File(os.path.join("", hdf5_file_name), 'w') as fast_h5:
        for i in range(POLAR):
            # transpose [channel, time] to [time, channel]
            fast_h5['polarization_%d' % i] = convert_data[i].T[::-1, :]
            # print("convert polar %d" % i)

    print("Done converting and stored into %s" % hdf5_file_name)
    return 0


def segment_hdf5_to_required_size(hdf5_file_name=None, after_segment_file_name=None, segment_row=None, segment_column=None, polars=None):
    """
    Function:
            segment hdf5 file into smaller parts
    :return:
    """
    if segment_row is None:
        segment_row = SEGMENT_ROW
    if segment_column is None:
        segment_column = SEGMENT_COLUMN
    if polars is None:
        polars = POLAR

    print("start to segment %s" % hdf5_file_name)
    segments_number_in_column = COLUMN // segment_column     # how many segments from the aspect of column
    segments_number = (COLUMN // segment_column) * (ROW // segment_row)
    with h5py.File(hdf5_file_name, 'r') as fast_h5:
        with h5py.File(after_segment_file_name, 'w') as file_for_write:
            for polar in range(polars):
                tod = fast_h5['polarization_%d' % polar][:, :]  # (65536, 2048)
                # print('polar %d' % polar)
                start_num = segments_number * polar

                for seg_num in range(segments_number):
                    row = seg_num // segments_number_in_column
                    column = seg_num % segments_number_in_column

                    file_for_write['%05d' % (start_num + seg_num)] = tod[row * segment_row: (row + 1) * segment_row,
                                                 column * segment_column: (column + 1) * segment_column]
                    # print('polar %d segment %05d done with row %d and column %d' % (polar, seg_num, row, column))

            print('file segment done and stored into %s' % after_segment_file_name)

    return 0


def restore_segment_hdf5_files(hdf5_file_name=None, restored_file_name=None):

    restored_data = np.ones(shape=(ROW, COLUMN), dtype=np.uint8)  # (65536, 2048)
    segments_number_in_column = COLUMN // SEGMENT_COLUMN     # how many segments from the aspect of column
    print('start restoring file %s' % hdf5_file_name)
    with h5py.File(restored_file_name, 'w') as fast_h5, \
            h5py.File(hdf5_file_name, 'r') as file_for_read:
        for polar in range(POLAR):
            start_num = SEGMENTS_NUMBER * polar
            for seg_num in range(SEGMENTS_NUMBER):
                row = seg_num // segments_number_in_column
                column = seg_num % segments_number_in_column
                restored_data[row * SEGMENT_ROW: (row + 1) * SEGMENT_ROW,
                    column * SEGMENT_COLUMN: (column + 1) * SEGMENT_COLUMN] = file_for_read['%05d' % (start_num + seg_num)]
                # print('polar %d segment %04d done' % (polar, seg_num))
            fast_h5['polarization_%d' % polar] = restored_data
        print('restored file stored into %s' % restored_file_name)

    return 0

