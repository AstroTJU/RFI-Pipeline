#include <bits/stdc++.h>
#include <getopt.h>
#include <unistd.h>

#define FILE_DIR_LENGTH 100

char stored_file_node[20] = "TJU-FAST-RFI";
char node_name[FILE_DIR_LENGTH];	// the node that process is running at
char dir_to_process[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set"; // dir in which there are files to be processed
char dir_to_store_result[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set/processed_results";
char workflow_executor[FILE_DIR_LENGTH] = "workflow.py";
char py_interpreter[FILE_DIR_LENGTH] = "~/anaconda3/envs/base/bin/python";
int files_num_to_process = 0;

int main(int argc, char *argv[])
{
    int opt;
    int digit_optind = 0;
    int option_index = 0;
    char *string = "a:b:c:d:e:f:";
	
    static struct option long_options[] =
    {  
        {"dir_to_process",			required_argument, NULL, 'a'},
        {"dir_to_store_result",		required_argument, NULL, 'b'},
        {"stored_file_node",		required_argument, NULL, 'c'},
        {"files_num_to_process",	required_argument, NULL, 'd'},
		{"workflow_executor",		required_argument, NULL, 'e'},
		{"py_interpreter",			required_argument, NULL, 'f'}
    }; 
	
	// ./getopt --workflow_executor worker.py
    while((opt =getopt_long_only(argc,argv,string,long_options,&option_index))!= -1)
    {  
        // printf("opt = %c\t\t", opt);
        // printf("optarg = %s\t\t",optarg);
        // printf("optind = %d\t\t",optind);
        // printf("argv[optind] =%s\t\t", argv[optind]);
        // printf("option_index = %d\n",option_index);
		
		switch(opt) {
            case 'a':
                strncpy(dir_to_process, optarg, FILE_DIR_LENGTH);
                break;
            case 'b':
                strncpy(dir_to_store_result, optarg, FILE_DIR_LENGTH);
                break;
            case 'c':
                strncpy(stored_file_node, optarg, FILE_DIR_LENGTH);
                break;
            case 'd':
                files_num_to_process = atoi(optarg);
                break;
			case 'e':
                strncpy(workflow_executor, optarg, FILE_DIR_LENGTH);
                break;
			case 'f':
                strncpy(py_interpreter, optarg, FILE_DIR_LENGTH);
                break;
			default:
				printf("unknow input arg %s", argv[optind]);
				return 1;
        }
    }
	
	printf("dir_to_process:%s\ndir_to_store_result:%s\nstored_file_node:%s\nprocess_files_num:%d\nworkflow_executor:%s\npy_interpreter:%s\n", dir_to_process, dir_to_store_result, stored_file_node, files_num_to_process, workflow_executor, py_interpreter);
}





