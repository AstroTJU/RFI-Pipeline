# -*- coding:utf-8 -*-
"""  
#====#====#====#====
# Project Name:     RFI-Net
# File Name:        Thread
# Date:             2020/2/17 16:33 
# Using IDE:        PyCharm 
# From HomePage:    https://github.com/DuFanXin/RFI-Net
# Note:             
# Author:           DuFanXin 
# BlogPage:         http://blog.csdn.net/qq_30239975  
# E-mail:           18672969179@163.com
# Copyright (c) 2020, All Rights Reserved.
#====#====#====#==== 
"""
import threading
import time
# from utils import process_files
import numpy as np
from astropy.io import fits
import h5py
import os

# global exitFlag


class MyThread(threading.Thread):
    def __init__(self, thread_id, name, counter):
        threading.Thread.__init__(self)
        self.threadID = thread_id
        self.name = name
        self.counter = counter
        self.exitFlag = False

    def run(self):  # write code in run Function
        print("Starting " + self.name)
        # print(self.threadID)
        # self.print_time(0.5, 50)
        read_and_convert_fits_to_hdf5(
            fits_file_name="./Dec+0551_drifting-M19_W_0019_%d.fits" % self.threadID,
            hdf5_file_name="./convert_Dec+0551_drifting-M19_W_0019_%d.h5" % self.threadID
        )
        print("Exiting " + self.name)

    def print_time(self, delay, counter):
        while counter and not self.exitFlag:
            time.sleep(delay)
            print("%s: %s\n" % (self.name, time.ctime(time.time())))
            counter -= 1


POLAR = 2
ROW = 65536  # channel
COLUMN = 2048  # time
SEGMENT_ROW = 256
SEGMENT_COLUMN = 128
SEGMENTS_NUMBER = (COLUMN // SEGMENT_COLUMN) * (ROW // SEGMENT_ROW)
BAIS = 0.000001


def read_and_convert_fits_to_hdf5(fits_file_name=None, hdf5_file_name=None):
    """
    Function:
            read a fits file and extract data into a hdf5 file
    :param hdf5_file_name:
    :param fits_file_name:
    :return:
    """
    fast = fits.open(fits_file_name, lazy_load_hdus=True)
    # fast.info()
    # print(fast[1].data[1].field(20).shape)
    # print(fast[1].data.names)  # show every comment of each item

    # access item DATA
    data_shape = fast[1].data[1].field('DATA').shape
    data_row = data_shape[0]  # channel
    data_polar = data_shape[1]  # polarization
    data_column = fast[1].data.shape[0]  # time
    print("%s %d %d %d" % (fits_file_name, data_polar, data_row, data_column))

    # [polar, channel, time]
    convert_data = np.ones(shape=(data_polar, data_row, data_column), dtype=np.float64)
    for col in range(data_column):
        for polar in range(data_polar):
            convert_data[polar, :, col] = fast[1].data[col].field('DATA')[:, polar]

    with h5py.File(os.path.join("", hdf5_file_name), 'w') as fast_h5:
        for i in range(POLAR):
            # transpose [channel, time] to [time, channel]
            fast_h5['polarization_%d' % i] = convert_data[i].T[::-1, :]
            print("convert polar %d of %s" % (i, fits_file_name))

    print("Done converting, data is stored in hdf5 file %s" % hdf5_file_name)
    return 0


def main():
    # create new threads
    thread1 = MyThread(1, "Thread 1", 1)
    thread2 = MyThread(2, "Thread 2", 2)

    # start threads
    thread1.start()
    thread2.start()

    time.sleep(2)
    thread1.exitFlag = True
    thread2.exitFlag = True

    thread1.join()
    thread2.join()

    print("all threads ends")

    # read_and_convert_fits_to_hdf5(
    #     fits_file_name="./Dec+0551_drifting-M19_W_0019.fits",
    #     hdf5_file_name="./convert_Dec+0551_drifting-M19_W_0019.h5"
    # )


if __name__ == '__main__':
    main()
