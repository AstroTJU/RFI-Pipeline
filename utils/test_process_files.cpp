#include <bits/stdc++.h>
#include <string.h>
using namespace std;

#define COMMD_LENGTH 1024
#define FILE_DIR_LENGTH 100
#define READ_READY 		0
#define FILE_NOT_EXIST -4
#define FILE_DAMAGE	-5
int my_rank = -1;

char tmp_file_dir_for_process[FILE_DIR_LENGTH] = "tmp_dir_for_worker_test";
char dir_to_store_result[FILE_DIR_LENGTH] = "../../data_set/processed_results";

char* get_ctime()
{
	time_t now_time = time(NULL);
	static char time_string[26] = {'\0'};
	strncpy(time_string, ctime(&now_time), 26);
	time_string[24] = '\0';
	return time_string;
}

int process_files(char *file_path)
{
    char commd[COMMD_LENGTH];

	/*
		create dir to store result
	*/
	memset(commd, '\0', COMMD_LENGTH);
	sprintf(commd, "mkdir -p %s", dir_to_store_result);
	system(commd);

	/*
		create tmp dir to process files
	*/
	memset(commd, '\0', COMMD_LENGTH);
	sprintf(commd, "mkdir -p %s", tmp_file_dir_for_process);
	system(commd);

	/*
		separate file_name and file_dir from file_path
	*/
	string file_path_str = string(file_path);
	string::size_type index_of_name = file_path_str.find_last_of('/') + 1;
    string file_name = file_path_str.substr(index_of_name, file_path_str.length() - index_of_name);
    string file_dir = file_path_str.substr(0, index_of_name - 1);
    
	// printf("%s\tWorker %d is handling on %s\n", get_ctime(), my_rank, file_name.c_str());

	memset(commd, '\0', COMMD_LENGTH);
	// sprintf(commd, "~/anaconda3/envs/astro-py37/bin/python ../utils/workflow.py --data_dir=../../data_set --file_name=Dec+0551_drifting-M19_W_0018.fits --tmp_dir=tmp_dir_for_worker_2 --process_id=2");
	// printf("process %d start to detect RFI\n", my_rank);
	sprintf(commd, "~/anaconda3/envs/astro-py37/bin/python ../utils/workflow.py --data_dir=%s --file_name=%s --tmp_dir=%s --process_id=%d", file_dir.c_str(), file_name.c_str(), tmp_file_dir_for_process, my_rank);
	// printf("process %d start to detect RFI\n", my_rank);
	int return_value = system(commd);
	cout<<return_value<<endl;

    return 0;
}

int handelFile(char* file_path)
{
    int process_status = READ_READY;
	int return_value = 0;
	char commd[COMMD_LENGTH];

	/*
		separate file_name and file_dir from file_path
	*/
	string file_path_str = string(file_path);
	string::size_type index_of_name = file_path_str.find_last_of('/') + 1;
    string file_name = file_path_str.substr(index_of_name, file_path_str.length() - index_of_name);
    string file_dir = file_path_str.substr(0, index_of_name - 1);
    
	printf("%s\tWorker %d is handling on %s\n", get_ctime(), my_rank, file_name.c_str());

	memset(commd, '\0', COMMD_LENGTH);
	sprintf(commd, "~/anaconda3/envs/astro-py37/bin/python ../utils/workflow.py --data_dir=%s --file_name=%s --tmp_dir=%s --result_dir=%s --process_id=%d", file_dir.c_str(), file_name.c_str(), tmp_file_dir_for_process, dir_to_store_result, my_rank);
	// printf("process %d start to detect RFI\n", my_rank);
	return_value = system(commd);

	if(return_value == FILE_NOT_EXIST){
		process_status = FILE_NOT_EXIST;
	}

	if(return_value == FILE_DAMAGE){
		process_status = FILE_DAMAGE;
	}

	// memset(commd, '\0', COMMD_LENGTH);
	// sprintf(commd, "mv %s/%s_processed_by_%d.h5 %s/%s.h5", tmp_file_dir_for_process, file_name.c_str(), my_rank, dir_to_store_result, file_name.c_str());  // _processed_by_%d  , my_rank
	// printf("%s\n", commd);
	// system(commd);

	memset(commd, '\0', COMMD_LENGTH);
	sprintf(commd, "rm %s/%s*", tmp_file_dir_for_process, file_name.c_str());
	printf("%s\n", commd);
	// system(commd);

    // if(my_rank < 5){
	// 	sleep(50 + my_rank);
	// }
	// else{
	// 	sleep(60 + my_rank);
	// }

	printf("%s\tWorker %d has handled %s\n", get_ctime(), my_rank, file_name.c_str());
	return process_status;
}

int make_dir()
{
	char commd[COMMD_LENGTH];

	/*
		create dir to store result
	*/
	memset(commd, '\0', COMMD_LENGTH);
	sprintf(commd, "mkdir -p %s", dir_to_store_result);
	system(commd);

	/*
		create tmp dir to process files
	*/
	memset(commd, '\0', COMMD_LENGTH);
	sprintf(commd, "mkdir -p %s", tmp_file_dir_for_process);
	system(commd);

	return 0;
}

int main(int argc, char *argv[])
{
	char file_path[FILE_DIR_LENGTH] = "../../data_set/Dec+0551_drifting-M19_W_0019.fits";
	// ~/anaconda3/envs/astro-py37/bin/python workflow.py --data_dir=../../data_set --file_name=Dec+0551_drifting-M19_W_0018.fits --tmp_dir=tmp_dir_for_worker_2 --process_id=2
	make_dir();
    handelFile(file_path);
    return 0;
}
