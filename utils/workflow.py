# -*- coding:utf-8 -*-
"""  
#====#====#====#====
# Project Name:     RFI-Net
# File Name:        whole_pipeline
# Date:             2020/3/9 13:52 
# Using IDE:        PyCharm 
# From HomePage:    https://github.com/DuFanXin/RFI-Net
# Note:             
# Author:           DuFanXin 
# BlogPage:         http://blog.csdn.net/qq_30239975  
# E-mail:           18672969179@163.com
# Copyright (c) 2020, All Rights Reserved.
#====#====#====#==== 
"""
import unittest
# from utils import find_files
# from ddt import ddt, data, unpack
# from parameterized import parameterized
import sys
import argparse
import time

sys.path.append("..")
from utils import process_files

FILE_NOT_EXIST = -4
FILE_DAMAGE = -5

parser = argparse.ArgumentParser()


class Workflow():
    def read_and_convert_fits_to_hdf5(self, fits_file_name, hdf5_file_name):
        start_time = time.time()
        process_files.read_and_convert_fits_to_hdf5(
            fits_file_name=fits_file_name,
            hdf5_file_name=hdf5_file_name)
        print("done read and convert using %.2f" % (time.time() - start_time))

    def segment_hdf5_to_required_size(self, hdf5_file_name, after_segment_file_name):
        start_time = time.time()
        process_files.segment_hdf5_to_required_size(
            hdf5_file_name=hdf5_file_name,
            after_segment_file_name=after_segment_file_name)
        print("done segmenting using %.2f" % (time.time() - start_time))

    def rfi_net_predicted(self, predict_batch_size, need_predict_file_path, predict_result_path, model_file_path, start_index, data_num):
        from model import rfi_net_predict_separately
        start_time = time.time()
        net = rfi_net_predict_separately.RFI_Net()
        net.predict(predict_batch_size=predict_batch_size,
                    need_predict_file_path=need_predict_file_path,
                    predict_result_path=predict_result_path,
                    model_file_path=model_file_path,
                    start_index=start_index,
                    data_num=data_num)
        print("done predicting using %.2f" % (time.time() - start_time))

    def restore_segment_hdf5_files(self, hdf5_file_name, restored_file_name):
        start_time = time.time()
        process_files.restore_segment_hdf5_files(
            hdf5_file_name=hdf5_file_name,
            restored_file_name=restored_file_name)
        print("done restore using %.2f" % (time.time() - start_time))

def workflow_without_rfi_net():
    workflow = Workflow()
    print("start process files")
    start_time = time.time()
    args = parser.parse_args()
    data_dir = args.data_dir
    file_name = args.file_name
    tmp_dir = args.tmp_dir
    worker_id = args.worker_id
    result_dir = args.result_dir

    # try:
    workflow.read_and_convert_fits_to_hdf5(
        fits_file_name="%s/%s" % (data_dir, file_name),
        hdf5_file_name="%s/%s_converted_by_process_%d.h5" % (tmp_dir, file_name, worker_id))

    workflow.segment_hdf5_to_required_size(
        hdf5_file_name="%s/%s_converted_by_process_%d.h5" % (tmp_dir, file_name, worker_id),
        after_segment_file_name="%s/%s_segmented_by_process_%d.h5" % (tmp_dir, file_name, worker_id))

    # for i in range(4):
    #     workflow.rfi_net_predicted(
    #         predict_batch_size=64,
    #         need_predict_file_path="%s/%s_segmented_by_process_%d.h5" % (tmp_dir, file_name, worker_id),
    #         predict_result_path="%s/%s_predicted_by_process_%d.h5" % (tmp_dir, file_name, worker_id),
    #         model_file_path="../data_set/saved_models",
    #         start_index=2048*i,
    #         data_num=2048)
    
    workflow.restore_segment_hdf5_files(
        hdf5_file_name="%s/%s_segmented_by_process_%d.h5" % (tmp_dir, file_name, worker_id),
        restored_file_name="%s/%s.h5" % (result_dir, file_name))
        # restored_file_name="%s/%s_processed_by_%d.h5" % (result_dir, file_name, worker_id))   
        # restored_file_name="%s/%s_restored_by_process_%d.h5" % (tmp_dir, file_name, worker_id))

    # except:
    #     exit(FILE_DAMAGE)
    
    # finally:
    print("end process files, using %.2fs" % (time.time() - start_time))
    #     exit(0)

def workflow_with_rfi_net():
    workflow = Workflow()
    print("start process files")
    start_time = time.time()
    args = parser.parse_args()
    data_dir = args.data_dir
    file_name = args.file_name
    tmp_dir = args.tmp_dir
    worker_id = args.worker_id
    result_dir = args.result_dir
    model_dir = args.model_dir
    
    # try:
    workflow.read_and_convert_fits_to_hdf5(
        fits_file_name="%s/%s" % (data_dir, file_name),
        hdf5_file_name="%s/%s_converted_by_process_%d.h5" % (tmp_dir, file_name, worker_id))

    workflow.segment_hdf5_to_required_size(
        hdf5_file_name="%s/%s_converted_by_process_%d.h5" % (tmp_dir, file_name, worker_id),
        after_segment_file_name="%s/%s_segmented_by_process_%d.h5" % (tmp_dir, file_name, worker_id))

    for i in range(4):
        workflow.rfi_net_predicted(
            predict_batch_size=64,
            need_predict_file_path="%s/%s_segmented_by_process_%d.h5" % (tmp_dir, file_name, worker_id),
            predict_result_path="%s/%s_predicted_by_process_%d.h5" % (tmp_dir, file_name, worker_id),
            model_file_path=model_dir,  # ../data_set/saved_models
            start_index=2048*i,
            data_num=2048)
    
    workflow.restore_segment_hdf5_files(
        hdf5_file_name="%s/%s_predicted_by_process_%d.h5" % (tmp_dir, file_name, worker_id),
        restored_file_name="%s/%s.h5" % (result_dir, file_name))
        # restored_file_name="%s/%s_processed_by_%d.h5" % (result_dir, file_name, worker_id))   
        # restored_file_name="%s/%s_restored_by_process_%d.h5" % (tmp_dir, file_name, worker_id))

    # except:
        # exit(FILE_DAMAGE)
    
    # finally:
    print("end process files, using %.2fs" % (time.time() - start_time))
        # exit(0)

def main():
    workflow_without_rfi_net()


if __name__ == '__main__':

    # data path
    parser.add_argument('--data_dir', type=str, help='dir for input data')
    
    # file name
    parser.add_argument('--file_name', type=str, help='file`s name')
    
    # tmp file dir
    parser.add_argument('--tmp_dir', type=str, help='dir for tmp processed file')

    # result saved into
    parser.add_argument('--result_dir', type=str, help='output result path')

    # process ID
    parser.add_argument('--worker_id', type=int, help='process ID')

    # saved model
    parser.add_argument('--model_dir', type=str, help='dir of saved model')

    main()
