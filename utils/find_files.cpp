#include <bits/stdc++.h>
#include <string>
#include <vector>
#include <cstring>
#include <cstdio>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

using std::strcmp;
using std::string;
using std::vector;
using namespace std;

#if __cplusplus < 201103L
#define nullptr NULL
#endif



#define FILE_DIR_LENGTH 100

struct FileInfo
{
    string file_name;
    bool hasTouched;
    bool isError;
    FileInfo(string _file_name = "", bool _hasTouched = false, bool _isError = false) : file_name(_file_name), hasTouched(_hasTouched), isError(_isError) {}
};

queue<FileInfo> files_queue;
// #include<io.h>

// vector<string> files;

// void getFiles( string path, vector<string>& files )
// {
//     //文件句柄
//     long   hFile   =   0;
//     //文件信息
//     struct _finddata_t fileinfo;
//     string p;
//     if((hFile = _findfirst(p.assign(path).append("\\*").c_str(),&fileinfo)) !=  -1)
//     {
//         do
//         {
//             //如果是目录,迭代之
//             //如果不是,加入列表
//             if((fileinfo.attrib &  _A_SUBDIR))
//             {
//                 if(strcmp(fileinfo.name,".") != 0  &&  strcmp(fileinfo.name,"..") != 0)
//                     getFiles( p.assign(path).append("\\").append(fileinfo.name), files );
//             }
//             else
//             {
//                 files.push_back(p.assign(path).append("\\").append(fileinfo.name) );
//             }
//         }while(_findnsuffix(hFile, &fileinfo)  == 0);
//         _findclose(hFile);
//     }
// }

/*
*   搜索一个文件夹下的符合条件的文件
*/
int searchFile_backup(const char *directory, vector<string> &files, const char *suffix, bool recursive = false, int subdir_level = 1)
{
    const char *dir_path = directory;
    DIR *dir = nullptr;
    int file_num_count = 0;

    //打开文件夹
    if ((dir = opendir(dir_path)) == nullptr)
    {
        return false;
    }

    struct dirent entry;
    struct dirent *result;
    struct stat file_stat;

    char cur_name[FILE_DIR_LENGTH];

    //读取文件夹下的一个文件
    while (readdir_r(dir, &entry, &result) == 0 && result)
    {
        const char *name = entry.d_name;

        //忽略掉.和..
        if (strcmp(name, ".") == 0 || strcmp(name, "..") == 0)
            continue;

        //检查拼接路径
        int count = sprintf(cur_name, "%s", dir_path);

        count += sprintf(&cur_name[count], "%s%s", cur_name[count - 1] == '/' ? "" : "/", name);

        //获取文件的状态
        if (lstat(cur_name, &file_stat) != -1)
        {
            //文件是普通文件
            if (!S_ISDIR(file_stat.st_mode))
            {
                const char *rf = strrchr(cur_name, '.');

                //检查文件的后缀名
                if (rf != nullptr && strcmp(rf + 1, suffix) == 0)
                {
                    file_num_count++;
                    files.push_back(string(cur_name, count));
                }
            }
            //文件是文件夹
            else if (recursive && subdir_level >= 0) // subdir_level >= 0
            {
                //递归获取下一层目录的文件
                file_num_count += searchFile_backup(cur_name, files, suffix, recursive, subdir_level - 1);
            }
        }
    }
    closedir(dir);

    return file_num_count;
}

/*
   search files with specific suffix in a folder
   subdir_level to avoid endless recursion
*/
int searchFile(const char *directory, const char *suffix, bool recursive = false, int subdir_level = 1)
{
    const char *dir_path = directory;
    DIR *dir = nullptr;
    int file_num_count = 0;

    //  open a directory
    if ((dir = opendir(dir_path)) == nullptr)
    {
        return false;
    }

    struct dirent entry;
    struct dirent *result;
    struct stat file_stat;

    char cur_name[FILE_DIR_LENGTH];

    //  read a file in directory
    while (readdir_r(dir, &entry, &result) == 0 && result)
    {
        const char *name = entry.d_name;

        //  ignore . and ..
        if (strcmp(name, ".") == 0 || strcmp(name, "..") == 0)
            continue;

        int count = sprintf(cur_name, "%s", dir_path);
        count += sprintf(&cur_name[count], "%s%s", cur_name[count - 1] == '/' ? "" : "/", name);
        // cout<<cur_name<<endl;

        // read status of a file
        if (lstat(cur_name, &file_stat) != -1)
        {
            //  if is a file
            if (!S_ISDIR(file_stat.st_mode))
            {
                const char *rf = strrchr(cur_name, '.');

                //  check suffix
                if (rf != nullptr && strcmp(rf + 1, suffix) == 0)
                {
                    file_num_count++;
                    files_queue.push(FileInfo(string(cur_name, count)));
                }
            }
            else if (recursive && subdir_level >= 0) // if is a folder
            {
                //  retrieves the files of the next level directory recursively
                file_num_count += searchFile(cur_name, suffix, recursive, subdir_level - 1);
            }
        }
    }
    closedir(dir);

    return file_num_count;
}

int find_files()
{
    char *filePath = "../../data_set";
    // vector<string> files;

    // int count = searchFile(filePath, files, "fits", true, 100);
    // for(int i = 0; i < files.size(); i++){
    // 	cout<<files[i]<<endl;
    // }

    int count = searchFile(filePath, "fits", true, 100);
    while (!files_queue.empty())
    {
        string file_path = files_queue.front().file_name;
        string::size_type iPos = file_path.find_last_of('/') + 1;
        string filename = file_path.substr(iPos, file_path.length() - iPos);
        string filepath = file_path.substr(0, iPos - 1);
        cout << filename << endl;
        cout << filepath << endl;
        // cout<<files_queue.front().file_name<<endl;
        files_queue.pop();
    }
    cout << "end, find files " << count << endl;

    return 0;
}

int main(int argc, char *argv[])
{
    find_files();
    return 0;
}
