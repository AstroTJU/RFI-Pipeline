#include<bits/stdc++.h>
#include <unistd.h>
using namespace std;

#define FILE_DIR_LENGTH 100
#define COMMD_LENGTH 1024
// #define path = dir + file name

char stored_file_node[FILE_DIR_LENGTH] = "TJU-FAST-RFI";
char dir_to_process[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set"; // dir in which there are files to be processed
char dir_to_store_result[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set/processed_results";
int process_num = 0;
queue<string> file_names_queue;

int routine()
{
	char commd[300];
	// sprintf(commd, "#!/bin/bash && conda activate py37-tf113 && conda list && cd unit_test && python test_whole_pipeline_except_RFI-Net.py --data_dir=../data_set/Dec+0551_drifting-M19_W_0019.fits --process_id=1 --ip_address=127.0.0.1 && cd .. && conda deactivate");
	// sprintf(commd, "#!/bin/bash\n\nsource ~/anaconda3/etc/profile.d/conda.sh\nconda activate py37-tf113\nconda list\ncd unit_test\ncd ..\nconda deactivate");
	// printf("%s\n", commd);
	// system(commd);
	
	string file_path = "/home/YZC/projects/MPI/data_set/Dec+0551_drifting-M19_W_0018.fits";
	file_path = file_path.insert(file_path.find_last_of('/'), "/tmp_for_process");
	// cout<< file_path <<endl;
	sprintf(commd, "ssh %s \"mkdir -p %s/processed_results\" ", "TJU-FAST-RFI", "/home/YZC/projects/MPI");
	
	FILE *fp;
	char file_name[FILE_DIR_LENGTH];
	int total_file_num = 0;
	fp = popen("cd /home/YZC/projects/MPI/data_set && for entry in `ls Dec*`; do echo $entry\ndone", "r");
	while(fgets(file_name, sizeof(file_name), fp) != NULL){
		// printf("%d %s", total_file_num++, file_name);
		file_names_queue.push(file_name);
	}
	pclose(fp);
	
	while(!file_names_queue.empty()){
		printf("%s", file_names_queue.front().c_str());
		file_names_queue.pop();
	}
}

int main(int argc, char * argv[])
{
	if(argc == 2 && strcmp(argv[1], "-h") == 0){
		printf("Usage: \n\t./pipeline \n\t-i <Input file dir> \n\t-s <node name where stored file> \n\t-n <process number> \n\t-o <dir to store results>\n");
		return 0;
	}
	
	int opt = 0;
	while((opt = getopt(argc, argv, "i:n:o:")) != -1) {
        switch(opt) {
            case 'i':
                strncpy(dir_to_process, optarg, FILE_DIR_LENGTH);
                break;
            case 'o':
                strncpy(dir_to_store_result, optarg, FILE_DIR_LENGTH);
                break;
            case 's':
                strncpy(stored_file_node, optarg, FILE_DIR_LENGTH);
                break;
            case 'n':
                process_num = atoi(optarg);
                break;
			default:
				printf("unknow input arg %c", opt);
				return 1;
        }
    }
	// ./conda_test -i a -o b -n 5
	printf("%s\n%s\n%s\n%d\n", dir_to_process, dir_to_store_result, stored_file_node, process_num);
	
	routine();
	
	return 0;
}
