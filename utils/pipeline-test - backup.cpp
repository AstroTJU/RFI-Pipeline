#include <mpi.h>
#include <unistd.h>
#include <bits/stdc++.h>
#include <pthread.h>
using namespace std;

#define READ_READY 		0
#define READY_FOR_FILE	1
#define PROCESS_DONE 	-1
#define PROCESS_ERROR	-2
#define PROCESS_EXIT	-3
#define FILE_DIR_LENGTH 100
#define COMMD_LENGTH 1024
#define TIME_INTERVAL	5	// every time_interval, the manager tries to contact workers
#define TIME_COMMUNICATE_DEADLINE_LINE	TIME_INTERVAL*5	// consider lose contact
#define TIME_MIN_FOR_PROCESSING	500	// 4 within this time is considerred not finishing process
#define TIME_MAX_FOR_PROCESSING	1000	// 10 within this time is considerred normal
#define TIMES_FOR_RETRY	5	// the times that manager trys to contact workers
#define HEART_BEAT_TAG	1
// #define path = dir + file name

struct WorkerHandle{
	int worker_rank;
	string file_name;
	long last_comunic_time;
	long assign_time;
	WorkerHandle(int _worker_rank, string _file_name="", long _last_comunic_time=time(NULL), long _assign_time=0):worker_rank(_worker_rank), file_name(_file_name), last_comunic_time(_last_comunic_time), assign_time(_assign_time){}
};

queue<string> file_names_queue;
queue<int> process_queue;

vector<string> process_to_file;
vector<WorkerHandle> workers;
vector<pthread_mutex_t> mutex_locks;

MPI_Status status;

char ip[20] = "127.0.0.1";
char stored_file_node[20] = "TJU-FAST-RFI";
char node_name[FILE_DIR_LENGTH];	// the node that process is running at
char dir_to_process[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set"; // dir in which there are files to be processed
char dir_to_store_result[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set/processed_results";

ofstream error_log("error.log");
ofstream run_log("run.log");

int files_num_to_process = 0;
int my_rank; // current process ID
int np; // total number of processes
int name_len;
int tag = 0; // communication marks

bool thread_terminate = true;

void* hello(void* rank);
void* manager_thread(void* rank);
void* worker_thread(void* rank);
void* hello_sleep(void* rank);
void* hello_isend_irecv_manager_single_thread_loop(void* rank);
void* hello_isend_irecv_worker_loop(void* rank);

int send_file_name_backup(int process_id)
{
	int process_status;
	if(!file_names_queue.empty()){
		process_status = READY_FOR_FILE;
		MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);

		char file_name[FILE_DIR_LENGTH];
		strncpy(file_name, file_names_queue.front().c_str(), FILE_DIR_LENGTH);
		printf("sending file name %s to worker %d\n", file_name, process_id);
		MPI_Send(file_name, FILE_DIR_LENGTH, MPI_CHAR, process_id, tag, MPI_COMM_WORLD);

		process_to_file[process_id] = file_names_queue.front();
		file_names_queue.pop();
		
		process_queue.push(process_id);
	}
	else{
		process_status = PROCESS_DONE;
		MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
		// run_log<<"process "<<process_id<<" is done working"<<endl;
	}

	return 0;
}

int send_file_name(int process_id)
{
	int process_status;
	if(!file_names_queue.empty()){
		process_status = READY_FOR_FILE;
		MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);

		char file_name[FILE_DIR_LENGTH];
		strncpy(file_name, file_names_queue.front().c_str(), FILE_DIR_LENGTH);
		printf("sending file name %s to worker %d\n", file_name, process_id);
		MPI_Send(file_name, FILE_DIR_LENGTH, MPI_CHAR, process_id, tag, MPI_COMM_WORLD);

		// process_to_file[process_id] = file_names_queue.front();
		workers[process_id].file_name = file_name;
		workers[process_id].assign_time = time(NULL);
		file_names_queue.pop();
		
		process_queue.push(process_id);
	}
	else{
		process_status = PROCESS_DONE;
		printf("sending jobs done to worker %d\n", process_id);
		MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
		run_log<<"process "<<process_id<<" is done working"<<endl;
	}

	return 0;
}

int manager_backup()
{
	for(int i = 0; i < files_num_to_process; i++){
		// char file_dir[FILE_DIR_LENGTH];
		// sprintf(file_dir, "Dec+0551_drifting-M19_W_00%d.fits", 18 + i);
		// string file_dir_s(file_dir);
		// file_names_queue.push(file_dir_s);
		file_names_queue.push("Dec+0551_drifting-M19_W_0018.fits");
	}

    for(int i = 1; i < np; i++){
        process_queue.push(i);
    }

    while(!process_queue.empty()){
        int process_id = process_queue.front();
		printf("assigning jobs to worker %d.\n", process_id);
        int process_status = 0;
        process_queue.pop();
        MPI_Recv(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD, &status);

        if(process_status == READ_READY){
            send_file_name(process_id);
        }

        if(process_status == PROCESS_ERROR){
            // add error message to log file
            // error_log<<"something wrong with "<<process_to_file[process_id]<<endl;
            send_file_name(process_id);
        }
    }
    // commit log file
    printf("all procedures done, committing log files\n");
}

int manager_backup_2()
{
	int thread_count = 1;
	
	for(int i = 0; i < files_num_to_process; i++){
		// char file_dir[FILE_DIR_LENGTH];
		// sprintf(file_dir, "Dec+0551_drifting-M19_W_00%d.fits", 18 + i);
		// string file_dir_s(file_dir);
		// file_names_queue.push(file_dir_s);
		file_names_queue.push("Dec+0551_drifting-M19_W_0018.fits");
	}
	
	/*
		thread start
	*/
	mutex_locks.resize(np);
	pthread_t* thread_handles = NULL;
	thread_handles = (pthread_t*)malloc(thread_count * sizeof(pthread_t));
	thread_terminate = false;

	for(int i = 0; i < np; i++){
		WorkerHandle tmpWorker(i);
        workers.push_back(tmpWorker);
		pthread_mutex_init(&mutex_locks[i], NULL);
		// pthread_create(&thread_handles[i], NULL, hello_sleep, (void*)(long)i);
    }
	
	for(int i = 0; i < thread_count; i++){
		// pthread_mutex_init(&mutex_locks[i], NULL);
		pthread_create(&thread_handles[i], NULL, hello_isend_irecv_manager_single_thread_loop, (void*)(long)i);
    }

    for(int i = 1; i < np; i++){
        process_queue.push(i);
    }
	
	/*
		assign task to workers
	*/
    while(!process_queue.empty()){
        int process_id = process_queue.front();
        int process_status = 0;
		long now_time = time(NULL);
        process_queue.pop();

		if(now_time - workers[process_id].assign_time < TIME_MIN_FOR_PROCESSING){
			process_queue.push(process_id);
			printf("time is not up, pushing %d back to queue. now time %ld, ass_time %ld, diff %ld\n", process_id, now_time, workers[process_id].assign_time, now_time - workers[process_id].assign_time);
			sleep(3);
			continue;
		}
		
		// if(now_time - worker[process_id].assign_time > )
		
        MPI_Recv(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD, &status);
		printf("dealing with report from worker %d.\n", process_id);
        if(process_status == READ_READY){
            send_file_name(process_id);
        }

        if(process_status == PROCESS_ERROR){
            // add error message to log file
            // error_log<<"something wrong with "<<process_to_file[process_id]<<endl;
            send_file_name(process_id);
        }
    }
	
	/*
		thread end
	*/
	thread_terminate = true;
	for(int i = 0; i < thread_count; i++){
		pthread_join(thread_handles[i], NULL);
	}

	for(int i = 0; i < np; i++){
		pthread_mutex_destroy(&mutex_locks[i]);
	}

	free(thread_handles);
	thread_handles = NULL;
    // commit log file
    printf("all procedures done, committing log files\n");
}

int manager_backup_3()
{
	int thread_count = 1;
	
	for(int i = 0; i < files_num_to_process; i++){
		// char file_dir[FILE_DIR_LENGTH];
		// sprintf(file_dir, "Dec+0551_drifting-M19_W_00%d.fits", 18 + i);
		// string file_dir_s(file_dir);
		// file_names_queue.push(file_dir_s);
		file_names_queue.push("Dec+0551_drifting-M19_W_0018.fits");
	}
	
	/*
		thread start
	*/
	mutex_locks.resize(np);
	pthread_t* thread_handles = NULL;
	thread_handles = (pthread_t*)malloc(thread_count * sizeof(pthread_t));
	thread_terminate = false;

	for(int i = 0; i < np; i++){
		WorkerHandle tmpWorker(i);
        workers.push_back(tmpWorker);
		pthread_mutex_init(&mutex_locks[i], NULL);
		// pthread_create(&thread_handles[i], NULL, hello_sleep, (void*)(long)i);
    }

	for(int i = 0; i < thread_count; i++){
		// pthread_mutex_init(&mutex_locks[i], NULL);
		pthread_create(&thread_handles[i], NULL, hello_isend_irecv_manager_single_thread_loop, (void*)(long)i);
    }

    for(int i = 1; i < np; i++){
        process_queue.push(i);
    }
	
	/*
		assign task to workers
	*/
    while(!process_queue.empty()){
        int process_id = process_queue.front();
        int process_status = 0;
		long now_time = time(NULL), last_comunic_time = 0;
        process_queue.pop();
		
		/* if running time is less than assumed process time, it is considered that processing is not finish, so will not recv the respond */
		if(now_time - workers[process_id].assign_time < TIME_MIN_FOR_PROCESSING){
			process_queue.push(process_id);
			printf("time is not up, pushing %d back to queue. now time %ld, ass_time %ld, diff %ld\n", process_id, now_time, workers[process_id].assign_time, now_time - workers[process_id].assign_time);
			sleep(1);
			continue;
		}
		
		/* if the worker is not responding for a long enough time, it is considered that the worker is not running normaly, so it will be termimnated */
		pthread_mutex_lock(&mutex_locks[process_id]);
			last_comunic_time = workers[process_id].last_comunic_time;
		pthread_mutex_unlock(&mutex_locks[process_id]);
		
		if(now_time - last_comunic_time > TIME_COMMUNICATE_DEADLINE_LINE){
			// will not push process_id back to the queue
			// but push file_name back to file_queue
			file_names_queue.push(workers[process_id].file_name);
			process_status = PROCESS_DONE;
			MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
			printf("process %d is not responding, might be down\n", process_id);
			continue;
		}

		/* if running time is more than assumed process time, it is considered that processing is somehow running unnormaly, so it will be terminated */
		if(workers[process_id].assign_time > 0 && now_time - workers[process_id].assign_time > TIME_MAX_FOR_PROCESSING){
			// will not push process_id back to the queue
			file_names_queue.push(workers[process_id].file_name);
			process_status = PROCESS_DONE;
			MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
			printf("process %d has running for too much time, it will be terminated\n", process_id);
			sleep(1);
			continue;
		}
		
        MPI_Recv(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD, &status);
		printf("dealing with report from worker %d.\n", process_id);
        if(process_status == READ_READY){
            send_file_name(process_id);
        }

        if(process_status == PROCESS_ERROR){
            // add error message to log file
            // error_log<<"something wrong with "<<process_to_file[process_id]<<endl;
            send_file_name(process_id);
        }
    }
	
	/*
		thread end
	*/
	thread_terminate = true;
	for(int i = 0; i < thread_count; i++){
		pthread_join(thread_handles[i], NULL);
	}

	for(int i = 0; i < np; i++){
		pthread_mutex_destroy(&mutex_locks[i]);
	}

	free(thread_handles);
	thread_handles = NULL;
    // commit log file
    printf("all procedures done, committing log files\n");
}

int manager_backup_4()
{
	int thread_count = 1;
	
	for(int i = 0; i < files_num_to_process; i++){
		// char file_dir[FILE_DIR_LENGTH];
		// sprintf(file_dir, "Dec+0551_drifting-M19_W_00%d.fits", 18 + i);
		// string file_dir_s(file_dir);
		// file_names_queue.push(file_dir_s);
		file_names_queue.push("Dec+0551_drifting-M19_W_0018.fits");
	}
	
	/*
		thread start
	*/
	mutex_locks.resize(np);
	pthread_t* thread_handles = NULL;
	thread_handles = (pthread_t*)malloc(thread_count * sizeof(pthread_t));
	thread_terminate = false;

	for(int i = 0; i < np; i++){
		WorkerHandle tmpWorker(i);
        workers.push_back(tmpWorker);
		pthread_mutex_init(&mutex_locks[i], NULL);
		// pthread_create(&thread_handles[i], NULL, hello_sleep, (void*)(long)i);
    }

	for(int i = 0; i < thread_count; i++){
		// pthread_mutex_init(&mutex_locks[i], NULL);
		pthread_create(&thread_handles[i], NULL, hello_isend_irecv_manager_single_thread_loop, (void*)(long)i);
    }

    for(int i = 1; i < np; i++){
        process_queue.push(i);
    }
	
	/*
		assign task to workers
	*/
    while(!process_queue.empty()){
        int process_id = process_queue.front();
        int process_status = 0, flag = 0;
		MPI_Request request;
		long now_time = time(NULL), last_comunic_time = 0;
        process_queue.pop();
		
		/* if running time is less than assumed process time, it is considered that processing is not finish, so will not recv the respond */
		if(now_time - workers[process_id].assign_time < TIME_MIN_FOR_PROCESSING){
			process_queue.push(process_id);
			printf("time is not up, pushing %d back to queue. now time %ld, ass_time %ld, diff %ld\n", process_id, now_time, workers[process_id].assign_time, now_time - workers[process_id].assign_time);
			sleep(1);
			continue;
		}
		
		/* if the worker is not responding for a long enough time, it is considered that the worker is not running normaly, so it will be termimnated */
		pthread_mutex_lock(&mutex_locks[process_id]);
			last_comunic_time = workers[process_id].last_comunic_time;
		pthread_mutex_unlock(&mutex_locks[process_id]);
		
		if(now_time - last_comunic_time > TIME_COMMUNICATE_DEADLINE_LINE){
			// will not push process_id back to the queue
			// but push file_name back to file_queue
			file_names_queue.push(workers[process_id].file_name);
			process_status = PROCESS_DONE;
			MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
			printf("process %d is not responding, might be down\n", process_id);
			continue;
		}

		/* if running time is more than assumed process time, it is considered that processing is somehow running unnormaly, so it will be terminated */
		if(workers[process_id].assign_time > 0 && now_time - workers[process_id].assign_time > TIME_MAX_FOR_PROCESSING){
			// will not push process_id back to the queue
			file_names_queue.push(workers[process_id].file_name);
			process_status = PROCESS_DONE;
			MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
			printf("process %d has running for too much time, it will be terminated\n", process_id);
			sleep(1);
			continue;
		}
		
		/* check the whether worker hs finished processing. if not push it back to process_queue; otherwise, handle the report */
        // MPI_Recv(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD, &status);
		MPI_Irecv(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD, &request);
		for(int times = 0; times < TIMES_FOR_RETRY; times++){
			MPI_Test(&request, &flag, &status);
			if(flag == 0){
				printf("manager notice worker %d not finish for No.%d\n", process_id, times + 1);
				sleep(1);
			}
			else{
				printf("manager notice worker %d finished\n", process_id);
				break;
			}
		}
		/*worker has not finish*/
		if(flag == 0){
			process_queue.push(process_id);
			printf("manager notice worker %d is not ready, so push it back to queue\n", process_id);
			continue;
		}
		
		printf("dealing with report from worker %d.\n", process_id);
        if(process_status == READ_READY){
            send_file_name(process_id);
        }

        if(process_status == PROCESS_ERROR){
            // add error message to log file
            // error_log<<"something wrong with "<<process_to_file[process_id]<<endl;
            send_file_name(process_id);
        }
    }
	
	/*
		thread end
	*/
	thread_terminate = true;
	for(int i = 0; i < thread_count; i++){
		pthread_join(thread_handles[i], NULL);
	}

	for(int i = 0; i < np; i++){
		pthread_mutex_destroy(&mutex_locks[i]);
	}

	free(thread_handles);
	thread_handles = NULL;
    // commit log file
    printf("all procedures done, committing log files\n");
}

int manager()
{
	int thread_count = 1;
	
	for(int i = 0; i < files_num_to_process; i++){
		// char file_dir[FILE_DIR_LENGTH];
		// sprintf(file_dir, "Dec+0551_drifting-M19_W_00%d.fits", 18 + i);
		// string file_dir_s(file_dir);
		// file_names_queue.push(file_dir_s);
		file_names_queue.push("Dec+0551_drifting-M19_W_0018.fits");
	}
	
	/*
		thread start
	*/
	mutex_locks.resize(np);
	pthread_t* thread_handles = NULL;
	thread_handles = (pthread_t*)malloc(thread_count * sizeof(pthread_t));
	thread_terminate = false;

	for(int i = 0; i < np; i++){
		WorkerHandle tmpWorker(i);
        workers.push_back(tmpWorker);
		pthread_mutex_init(&mutex_locks[i], NULL);
		// pthread_create(&thread_handles[i], NULL, hello_sleep, (void*)(long)i);
    }

	for(int i = 0; i < thread_count; i++){
		// pthread_mutex_init(&mutex_locks[i], NULL);
		pthread_create(&thread_handles[i], NULL, hello_isend_irecv_manager_single_thread_loop, (void*)(long)i);
    }

    for(int i = 1; i < np; i++){
        process_queue.push(i);
    }
	
	/*
		assign task to workers
	*/
    while(!process_queue.empty()){
        int process_id = process_queue.front();
        int process_status = 0, flag = 0;
		MPI_Request request;
		long now_time = time(NULL), last_comunic_time = 0;
        process_queue.pop();
		
		/* if running time is less than assumed process time, it is considered that processing is not finish, so will not recv the respond */
		if(now_time - workers[process_id].assign_time < TIME_MIN_FOR_PROCESSING){
			process_queue.push(process_id);
			printf("time is not up, pushing %d back to queue. now time %ld, ass_time %ld, diff %ld\n", process_id, now_time, workers[process_id].assign_time, now_time - workers[process_id].assign_time);
			sleep(10 + (TIME_MIN_FOR_PROCESSING - now_time + workers[process_id].assign_time) / np);
			continue;
		}
		
		/* if the worker is not responding for a long enough time, it is considered that the worker is not running normaly, so it will be termimnated */
		pthread_mutex_lock(&mutex_locks[process_id]);
			last_comunic_time = workers[process_id].last_comunic_time;
		pthread_mutex_unlock(&mutex_locks[process_id]);
		
		if(now_time - last_comunic_time > TIME_COMMUNICATE_DEADLINE_LINE){
			// will not push process_id back to the queue
			// but push file_name back to file_queue
			file_names_queue.push(workers[process_id].file_name);
			process_status = PROCESS_DONE;
			MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
			printf("process %d is not responding, might be down\n", process_id);
			continue;
		}

		/* if running time is more than assumed process time, it is considered that processing is somehow running unnormaly, so it will be terminated */
		if(workers[process_id].assign_time > 0 && now_time - workers[process_id].assign_time > TIME_MAX_FOR_PROCESSING){
			// will not push process_id back to the queue
			file_names_queue.push(workers[process_id].file_name);
			process_status = PROCESS_DONE;
			MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
			printf("process %d has running for too much time, it will be terminated\n", process_id);
			sleep(1);
			continue;
		}
		
		/* check the whether worker has finished processing. if not push it back to process_queue; otherwise, handle the report */
        // MPI_Recv(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD, &status);
		MPI_Irecv(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD, &request);
		for(int times = 0; times < TIMES_FOR_RETRY; times++){
			MPI_Test(&request, &flag, &status);
			if(flag == 0){
				printf("manager notice worker %d not finish for No.%d\n", process_id, times + 1);
				sleep(1);
			}
			else{
				printf("manager notice worker %d finished\n", process_id);
				break;
			}
		}
		/*worker has not finish*/
		if(flag == 0){
			process_queue.push(process_id);
			printf("manager notice worker %d is not ready, so push it back to queue\n", process_id);
			continue;
		}
		
		printf("dealing with report from worker %d.\n", process_id);
		if(process_status == READ_READY){
            run_log<<workers[process_id].file_name<<" has been processed by worker "<<process_id<<endl;
        }

        if(process_status == PROCESS_ERROR){
            // add error message to log file
            error_log<<"something wrong with "<<workers[process_id].file_name<<endl;
            // send_file_name(process_id);
        }
		
		send_file_name(process_id);
    }
	
	/*
		thread end
	*/
	thread_terminate = true;
	for(int i = 0; i < thread_count; i++){
		pthread_join(thread_handles[i], NULL);
	}

	for(int i = 0; i < np; i++){
		pthread_mutex_destroy(&mutex_locks[i]);
	}

	free(thread_handles);
	thread_handles = NULL;
    // commit log file
    printf("all procedures done, committing log files\n");
}

int worker()
{
    int process_status = READ_READY;
    MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
    bool something_wrong = false;
	int thread_count = 1;
	
	/*
		thread start
	*/
	pthread_t* thread_handles = NULL;
	thread_handles = (pthread_t*)malloc(thread_count * sizeof(pthread_t));
	thread_terminate = false;
	for(int i = 0; i < thread_count; i++){
		// pthread_mutex_init(&mutex_locks[i], NULL);
		pthread_create(&thread_handles[i], NULL, hello_isend_irecv_worker_loop, (void*)(long)i);
    }
	
	/*
		process files
	*/
	char commd[COMMD_LENGTH]; // make directory for tmp results
	sprintf(commd, "mkdir -p %s", dir_to_store_result);
	// system(commd);

    while(process_status != PROCESS_DONE){
        MPI_Recv(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);

        if(process_status == PROCESS_DONE){
            // do nothing
            process_status = PROCESS_EXIT;
            // MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            break;
        }

        /* handle file */
        if(process_status == READY_FOR_FILE){
			char file_name[FILE_DIR_LENGTH];
            MPI_Recv(file_name, FILE_DIR_LENGTH, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);

			// handle files
			if(my_rank == 1){
				sleep(569);
				something_wrong = true;
			}
			
			if(my_rank == 2){
				sleep(523);
			}

            if(something_wrong){
                process_status = PROCESS_ERROR;
                //MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            }
            else{
				process_status = READ_READY;
                //MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            }
			printf("worker %d has handled %s\n", my_rank, file_name);
			MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
        }
    }
	
	/*
		thread end
	*/
	printf("worker %d starts ending thread\n", my_rank);
	thread_terminate = true;
	for(int i = 0; i < thread_count; i++){
		pthread_join(thread_handles[i], NULL);
		// pthread_mutex_destroy(&mutex_locks[i]);
	}
	free(thread_handles);
	thread_handles = NULL;

    printf("Work of Process %d is done\n", my_rank);
}

int isend_recv_routine()
{
    int isbuf, irbuf, count;
	int tag = 0, flag = 0;
    MPI_Request request;
    MPI_Status status;
	if(my_rank == 0) {
        isbuf = 9;
		sleep(4);
        // MPI_Isend( &isbuf, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, &request);
		MPI_Send(&isbuf, 1, MPI_INT, 1, tag, MPI_COMM_WORLD);
    }
	else{
		sleep(3);
        MPI_Irecv(&irbuf, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &request);
        // MPI_Wait(&request, &status);
		MPI_Test(&request, &flag, &status);
        // MPI_Get_count(&status, MPI_INT, &count);
        // printf("irbuf = %d source = %d tag = %d count = %d\n", irbuf, status.MPI_SOURCE, status.MPI_TAG, count);
		if(flag == 0){
			printf("nothing received\n");
		}
		else{
			printf("irbuf = %d\n", irbuf);
		}

    }
}

int isend_recv_routine_for_loop()
{
    int isbuf, irbuf, count;
	int tag = 0, flag = 0;
    MPI_Request request;
    MPI_Status status;
	if(my_rank == 0) {
        isbuf = 9;
		sleep(4);
        // MPI_Isend( &isbuf, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, &request);
		MPI_Send(&isbuf, 1, MPI_INT, 1, tag, MPI_COMM_WORLD);
    }
	else if(my_rank == 1){
		MPI_Irecv(&irbuf, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &request);
		for(int i = 0; i < 5; i++){
			
			MPI_Test(&request, &flag, &status);
			if(flag == 0){
				printf("nothing received for %d\n", i + 1);
				sleep(3);
			}
			else{
				printf("irbuf = %d\n", irbuf);
				i = 5;
				break;
			}
		}
    }
	return 0;
}

int isend_recv_routine_for_loop_2()
{
    int isbuf, irbuf, count = 0;
	int tag = 0, flag = 0;
    MPI_Request request;
    MPI_Status status;
	if(my_rank == 0) {
        isbuf = 9;
		sleep(4);
        // MPI_Isend( &isbuf, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, &request);
		MPI_Send(&isbuf, 1, MPI_INT, 1, tag, MPI_COMM_WORLD);
    }
	else if(my_rank == 1){
		MPI_Irecv(&irbuf, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &request);
		for(int times = 0; times < 5; times++){
			MPI_Test(&request, &flag, &status);
			printf("flag %d\n", flag);
			if(flag == 0){
				printf("nothing received for %d\n", times + 1);
				sleep(3);
			}
			else{
				printf("irbuf = %d\n", irbuf);
				break;
			}
		}
		
		// while (1) { 
			// if(flag != 0)
			// {
				// MPI_Irecv(&irbuf, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &request);
				// flag = 0;
			// }
			// MPI_Test(&request, &flag, &status);

			// if (flag != 0) { 
				// printf("recv : %d, slave : %d\n", irbuf, status.MPI_SOURCE);
				// if (status.MPI_SOURCE != -1) 
					// count += irbuf;
				// flag = -1;
			// }


			// if (count == 9)
				// break;
		// }
    }
	return 0;
}

int test_routine()
{
	int MAX_STRING = 100;
	char greeting[MAX_STRING];
	if(my_rank==1){
		sprintf(greeting,"Greeting from process %d of %d!",my_rank,np);
		// sleep(10);
		MPI_Send(greeting,strlen(greeting)+1,MPI_CHAR,0,0,MPI_COMM_WORLD);
		printf("rank %d has returned from MPI_Send\n",my_rank);
	}else if(my_rank==2){
		sprintf(greeting,"Greeting from process %d of %d!",my_rank,np);
		MPI_Send(greeting,strlen(greeting)+1,MPI_CHAR,0,0,MPI_COMM_WORLD);
		printf("rank %d has returned from MPI_Send\n",my_rank);
	}else if(my_rank==3){
		sprintf(greeting,"Greeting from process %d of %d!",my_rank,np);
		MPI_Send(greeting,strlen(greeting)+1,MPI_CHAR,0,0,MPI_COMM_WORLD);
		printf("rank %d has returned from MPI_Send\n",my_rank);
	}else if(my_rank==0){
		//MPI_Recv(greeting,MAX_STRING,MPI_CHAR,1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		//printf("%s\n",greeting);
		MPI_Recv(greeting,MAX_STRING,MPI_CHAR,2,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		printf("%s\n",greeting);
		MPI_Recv(greeting,MAX_STRING,MPI_CHAR,3,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		printf("%s\n",greeting);
	}
	
	return 0;
}

void* hello(void* rank)
{
	long my_thread_rank = (long)rank;
	printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	return NULL;
}

void* hello_sleep(void* rank)
{
	long my_thread_rank = (long)rank;
	printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	while(!thread_terminate){
		sleep(2);
	}
	printf("Finished thread %ld of worker %d\n", my_thread_rank, my_rank);
	return NULL;
}

void* hello_send_recv_manager_single_thread(void* rank)
{
	long my_thread_rank = (long)rank;
	int tag = HEART_BEAT_TAG;
	char greeting[FILE_DIR_LENGTH];
	
	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	sprintf(greeting, "Greeting from thread %d of worker %d!", my_thread_rank, my_rank);
	for(int i = 1; i < np; i++){
		MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, i, tag, MPI_COMM_WORLD);
	}
	printf("send %s\n", greeting);
	
	sleep(1);
	
	for(int i = 1; i < np; i++){
		MPI_Recv(greeting, FILE_DIR_LENGTH, MPI_CHAR, i, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("thread %d of worker %d received %s\n", my_thread_rank, my_rank, greeting);
	}

	while(!thread_terminate){
		sleep(2);
	}
	printf("Finished thread %ld of worker %d\n", my_thread_rank, my_rank);
	return NULL;
}

void* hello_send_recv_worker(void* rank)
{
	long my_thread_rank = (long)rank;
	int tag = HEART_BEAT_TAG;
	char greeting[FILE_DIR_LENGTH];
	
	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	MPI_Recv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	printf("thread %d of worker %d received %s\n", my_thread_rank, my_rank, greeting);
	
	sleep(2);
	
	sprintf(greeting, "Greeting from thread %d of worker %d!", my_thread_rank, my_rank);
	MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
	printf("send %s\n", greeting);
	
	while(!thread_terminate){
		sleep(2);
	}
	printf("Finished thread %ld of worker %d\n", my_thread_rank, my_rank);
	return NULL;
}

void* hello_send_recv_manager_single_thread_loop(void* rank)
{
	long my_thread_rank = (long)rank;
	int tag = HEART_BEAT_TAG;
	char greeting[FILE_DIR_LENGTH];
	
	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	sprintf(greeting, "Greeting from thread %d of worker %d!", my_thread_rank, my_rank);
	

	while(!thread_terminate){
		for(int i = 1; i < np; i++){
			MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, i, tag, MPI_COMM_WORLD);
		}
		printf("send %s\n", greeting);
		
		sleep(1);
		
		for(int i = 1; i < np; i++){
			MPI_Recv(greeting, FILE_DIR_LENGTH, MPI_CHAR, i, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			
			pthread_mutex_lock(&mutex_locks[i]);
			workers[i].last_comunic_time = time(NULL);
			pthread_mutex_unlock(&mutex_locks[i]);
			
			printf("thread %d of worker %d received %s at %ld\n", my_thread_rank, my_rank, greeting, workers[i].last_comunic_time);
		}
		sleep(TIME_INTERVAL);
	}
	printf("Finished thread %ld of worker %d\n", my_thread_rank, my_rank);
	return NULL;
}

void* hello_isend_irecv_manager_single_thread_loop(void* rank)
{
	long my_thread_rank = (long)rank;
	int tag = HEART_BEAT_TAG, flag = 0;
    MPI_Request request;
    MPI_Status status;
	char greeting[FILE_DIR_LENGTH];
	
	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	sprintf(greeting, "Greeting from thread %d of worker %d!", my_thread_rank, my_rank);

	while(!thread_terminate){
		for(int i = 1; i < np; i++){
			MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, i, tag, MPI_COMM_WORLD);
		}
		printf("manager send %s\n", greeting);
		
		sleep(1);
		
		for(int i = 1; i < np; i++){
			
			// MPI_Recv(greeting, FILE_DIR_LENGTH, MPI_CHAR, i, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, i, tag, MPI_COMM_WORLD, &request);
			for(int times = 0; times < TIMES_FOR_RETRY; times++){
				MPI_Test(&request, &flag, &status);
				if(flag == 0){
					printf("thread %d of manager not received for %d from %d\n", my_thread_rank, times + 1, i);
					sleep(3);
				}
				else{
					
					pthread_mutex_lock(&mutex_locks[i]);
					workers[i].last_comunic_time = time(NULL);
					pthread_mutex_unlock(&mutex_locks[i]);
					
					printf("thread %d of worker %d received %s at %ld\n", my_thread_rank, my_rank, greeting, workers[i].last_comunic_time);
			
					times = TIMES_FOR_RETRY;
					break;
				}
			}
			
			// pthread_mutex_lock(&mutex_locks[i]);
			// workers[i].last_comunic_time = time(NULL);
			// pthread_mutex_unlock(&mutex_locks[i]);
			
			// printf("thread %d of worker %d received %s at %ld\n", my_thread_rank, my_rank, greeting, workers[i].last_comunic_time);
		}
		sleep(TIME_INTERVAL);
	}
	printf("Finished thread %ld of worker %d\n", my_thread_rank, my_rank);
	return NULL;
}

void* hello_send_recv_worker_loop(void* rank)
{
	long my_thread_rank = (long)rank;
	int tag = HEART_BEAT_TAG;
	char greeting[FILE_DIR_LENGTH];
	
	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	
	
	while(!thread_terminate){
		MPI_Recv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("thread %d of worker %d received %s\n", my_thread_rank, my_rank, greeting);
	
		sleep(1);
	
		sprintf(greeting, "Greeting from thread %d of worker %d!", my_thread_rank, my_rank);
		MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
		printf("send %s\n", greeting);
		sleep(TIME_INTERVAL);
	}
	printf("Finished thread %ld of worker %d\n", my_thread_rank, my_rank);
	return NULL;
}

void* hello_isend_irecv_worker_loop(void* rank)
{
	long my_thread_rank = (long)rank;
	int tag = HEART_BEAT_TAG, flag = 0;
    MPI_Request request;
    MPI_Status status;
	char greeting[FILE_DIR_LENGTH];
	
	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	
	
	while(!thread_terminate){
		// MPI_Recv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		// printf("thread %d of worker %d received %s\n", my_thread_rank, my_rank, greeting);
		
		MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &request);
		for(int times = 0; times < TIMES_FOR_RETRY; times++){
			MPI_Test(&request, &flag, &status);
			if(flag == 0){
				printf("thread %d of worker %d not received for No.%d from %d\n", my_thread_rank, my_rank, times + 1, 0);
				sleep(3);
			}
			else{
				printf("thread %d of worker %d received %s\n", my_thread_rank, my_rank, greeting);
		
				times = TIMES_FOR_RETRY;
				break;
			}
		}
	
		if(flag != 0){
			// sleep(1);
	
			sprintf(greeting, "Greeting from thread %d of worker %d!", my_thread_rank, my_rank);
			MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
			printf("send %s\n", greeting);
		}
		
		sleep(TIME_INTERVAL);
	}
	printf("Finished thread %ld of worker %d\n", my_thread_rank, my_rank);
	return NULL;
}

void* hello1(void* rank)
{
	// long my_thread_rank = (long)rank;
	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	// return NULL;
	
	long my_thread_rank = (long)rank;
	printf("%d on %d\n", my_thread_rank, my_rank);
	int MAX_STRING = 100;
	int tag = my_thread_rank;
	char greeting[MAX_STRING];
	if(my_thread_rank == 1){
		// sprintf(greeting,"Greeting from thread %d of worker %d!", my_thread_rank, my_rank);
		// MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 1, tag, MPI_COMM_WORLD);
		MPI_Recv(greeting, MAX_STRING, MPI_CHAR, 1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("thread %d of worker %d received %s\n", my_thread_rank, my_rank, greeting);
		// printf("send %s\n", greeting);
	}
	else{
		MPI_Recv(greeting, MAX_STRING, MPI_CHAR, 1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("thread %d of worker %d received %s\n", my_thread_rank, my_rank, greeting);
	}
	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	return NULL;
}

void* hello2(void* rank)
{
	long my_thread_rank = (long)rank;
	printf("%d on %d\n", my_thread_rank, my_rank);
	int MAX_STRING = 100;
	int tag = my_thread_rank ^ 1;
	char greeting[MAX_STRING];
	if(my_thread_rank == 0){
		sprintf(greeting,"Greeting from thread %d of worker %d!", my_thread_rank, my_rank);
		MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
		printf("send %s\n", greeting);
	}
	else{
		// MPI_Recv(greeting, MAX_STRING, MPI_CHAR, 0, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		sprintf(greeting,"Greeting from thread %d of worker %d!", my_thread_rank, my_rank);
		MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
		printf("send %s\n", greeting);
		// printf("thread %d of worker %d received %s\n", my_thread_rank, my_rank, greeting);
	}
	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	return NULL;
}

int thread_routine()
{
	long thread;
	pthread_t* thread_handles = NULL;
	
	int thread_count = 2;
	
	thread_handles = (pthread_t*)malloc(thread_count*sizeof(pthread_t));
	
	for(thread = 0; thread < thread_count; thread++){
		// pthread_create(&thread_handles[thread], NULL, hello, (void*)thread);
	}
	
	pthread_create(&thread_handles[0], NULL, hello, (void*)thread);
	pthread_create(&thread_handles[1], NULL, hello, (void*)thread);

	// if(my_rank == 0){
		// for(thread = 0; thread < thread_count; thread++){
			// pthread_create(&thread_handles[thread], NULL, hello1, (void*)thread);
		// }
	// }
	// else{
		// for(thread = 0; thread < thread_count; thread++){
			// pthread_create(&thread_handles[thread], NULL, hello2, (void*)thread);
		// }
	// }
	
	cout<<"create two threads"<<endl;
	
	for(thread = 0; thread < thread_count; thread++){
		pthread_join(thread_handles[thread], NULL);
	}
	
	free(thread_handles);
	thread_handles = NULL;
	
	return 0;
}

int process_routine()
{
	// process_to_file.resize(np);

	if(my_rank == 0){
	    manager();
	}
	else{
	    worker();
	}

	return 0;
}

int main(int argc, char *argv[])
{
	int provided;
	int start_time = time(NULL);
	MPI_Init_thread(&argc,&argv,MPI_THREAD_MULTIPLE, &provided); // initializes the MPI environment
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); // gets the current process ID
	MPI_Comm_size(MPI_COMM_WORLD, &np); // gets the total number of processes
	MPI_Get_processor_name(node_name, &name_len);
	
	printf("Process %d of %d is on %s\n", my_rank, np, node_name);

	if(argc == 2 && strcmp(argv[1], "-h") == 0){
		printf("Usage: \n\t./pipeline \n\t-i <Input file dir> \n\t-s <node name where stored file> \n\t-n <process number> \n\t-o <dir to store results>\n");
		return 0;
	}
	
	int opt = 0;
	while((opt = getopt(argc, argv, "i:n:o:")) != -1) {
        switch(opt) {
            case 'i':
                strncpy(dir_to_process, optarg, FILE_DIR_LENGTH);
                break;
            case 'o':
                strncpy(dir_to_store_result, optarg, FILE_DIR_LENGTH);
                break;
            case 's':
                strncpy(stored_file_node, optarg, FILE_DIR_LENGTH);
                break;
            case 'n':
                files_num_to_process = atoi(optarg);
                break;
			default:
				printf("unknow input arg %c", opt);
				return 1;
        }
    }

	if(my_rank == 0){
		printf("dir_to_process:%s\ndir_to_store_result:%s\nstored_file_node:%s\nprocess_files_num:%d\n", dir_to_process, dir_to_store_result, stored_file_node, files_num_to_process);
	}
	
	process_routine();
	// test_routine();
	// thread_routine();
	// isend_recv_routine();
	// isend_recv_routine_for_loop();

	//can MPI_BLOCK();
	MPI_Finalize();
	// int end_time = time(NULL);
	// cout<<end_time - start_time<<endl;
	return 0;
}

// yhrun -N 2 -n 4 -p th_mt1 ./mpi-2
// yhrun -N 2 -p th_mt1  mpirun -n 4 -f ./cpi_config ./mpi-2