#include <stdio.h>
#include <mpi.h>
#include <sys/time.h>

int main(int argc, char *argv[])
{

	int rank, size;
	MPI_Status status;

	/* Init */
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	if (rank != 0) { // Slaves
		int buf;

		if (rank == 1) {
			buf = 1;
			MPI_Send(&buf, 1, MPI_INT, 0, 0, MPI_COMM_WORLD); 
		}
		if (rank == 2) {
			buf = 2;
			MPI_Send(&buf, 1, MPI_INT, 0, 0, MPI_COMM_WORLD); 
		}

	}
	else { // Master
		int sum = 0;
		int flag = -1, res;
		MPI_Request request;
		MPI_Status status;
		while (1) { 
			if(flag != 0)
			{
				MPI_Irecv(&res, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &request);
				flag = 0;
			}
			MPI_Test(&request, &flag, &status);

			if (flag != 0) { 
				printf("recv : %d, slave : %d\n", res, status.MPI_SOURCE);
				if (status.MPI_SOURCE != -1) 
					sum += res;
				flag = -1;
			}


			if (sum == 3)
				break;
		}

		printf("sum : %d\n", sum);
	}

	MPI_Finalize();
	return 0;

}




"""
	pipeline scp
	file(s) num, 	1, 			2, 			4,			8,			16,			32
	node(s) num,
	1,	            522.235, 	1112.551, 	2155.089, 	4333.343, 	8692.549, 	17424.106 
	2, 	            NAN,        549.652, 	1093.354, 	2180.984, 	4374.794, 	8762.676 
	4, 	            NAN,        NAN,        583.105, 	1154.768, 	2293.773, 	4605.969 
	8, 	            NAN,		NAN,		NAN,        578.692, 	1185.106, 	2364.658 


	pipeline local
	file(s) num, 	1, 			2, 			4,			8,			16,			32
	node(s) num,
	1,				510.189,	1014.352,	2040.948‬,	4086.319,	8193.693‬,	16390.000
	2,				NAN,		511.533,	1020.222,	2044.901,	4082.519,	8188.095
	4,				NAN,		NAN,		535.909,	1062.707,	2119.013,	4241.723
	8,				NAN,		NAN,		NAN,		527.672,	1058.444,	2120.086
"""





