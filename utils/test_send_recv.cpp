#include <mpi.h>
#include <unistd.h>
#include <bits/stdc++.h>
#include <pthread.h>
using namespace std;

#define READ_READY 		0
#define READY_FOR_FILE	1
#define PROCESS_DONE 	-1
#define PROCESS_ERROR	-2
#define PROCESS_EXIT	-3
#define FILE_NOT_EXIST   -4
#define FILE_DAMAGE     -5
#define FILE_DIR_LENGTH 100
#define COMMD_LENGTH 1024
#define TIME_INTERVAL	5	// every time_interval, the manager tries to contact workers
#define TIME_COMMUNICATE_DEADLINE_LINE	TIME_INTERVAL*10	// consider lose contact
#define TIME_RELAX		3
#define TIME_MIN_FOR_PROCESSING	40	// 4 within this time is considerred not finishing process
#define TIME_MAX_FOR_PROCESSING	100	// 10 within this time is considerred normal
#define TIMES_FOR_RETRY	5	// the times that manager trys to contact workers
#define HEART_BEAT_TAG	1
// #define path = dir + file name

/*
for debug
*/
#define THREAD_DEBUG 				false
#define HEART_BEAT_STATUS_DEBUG 	false
#define MANAGER_DEBUG				true
#define WORKER_DEBUG				true

struct FileInfo{
	string file_name;
	bool hasTouched;
	bool isError;
	FileInfo(string _file_name="", bool _hasTouched=false, bool _isError=false): file_name(_file_name), hasTouched(_hasTouched), isError(_isError){}
};

struct WorkerHandle{
	int worker_rank;
	FileInfo file_info;
	long last_comunic_time;
	long assign_time;
	bool isPINGed;	// whether has been called by manager
	MPI_Request request;
	WorkerHandle(int _worker_rank, string _file_name="", long _last_comunic_time=time(NULL), long _assign_time=0, bool _isPINGed=false): worker_rank(_worker_rank), last_comunic_time(_last_comunic_time), assign_time(_assign_time), isPINGed(_isPINGed){
		file_info.file_name = _file_name;
	}
};

queue<FileInfo> files_queue;
queue<int> process_queue;

vector<string> process_to_file;
vector<WorkerHandle> workers;
vector<pthread_mutex_t> mutex_locks;

MPI_Status status;

char ip[20] = "127.0.0.1";
char stored_file_node[20] = "TJU-FAST-RFI";
char node_name[FILE_DIR_LENGTH];	// the node that process is running at
char dir_to_process[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set"; // dir in which there are files to be processed
char dir_to_store_result[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set/processed_results";

ofstream error_log("error.log");
ofstream run_log("run.log");

int files_num_to_process = 0;
int my_rank; // current process ID
int np; // total number of processes
int name_len;
int tag = 0; // communication marks

bool thread_terminate = true;

void* hello(void* rank);
int handelFile(char *file_name);
char* get_ctime();

int send_file_name(int process_id)
{
	int process_status;
	if(!files_queue.empty()){
		process_status = READY_FOR_FILE;
		MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);

		char file_name[FILE_DIR_LENGTH];
		strncpy(file_name, files_queue.front().file_name.c_str(), FILE_DIR_LENGTH);
		printf("Manager is assigning %s to worker %d\n", file_name, process_id);
		MPI_Send(file_name, FILE_DIR_LENGTH, MPI_CHAR, process_id, tag, MPI_COMM_WORLD);

		// process_to_file[process_id] = files_queue.front();
		workers[process_id].file_info = files_queue.front();
		workers[process_id].assign_time = time(NULL);
		files_queue.pop();

		process_queue.push(process_id);
	}
	else{
		process_status = PROCESS_DONE;
		printf("Manager is sending jobs done to worker %d\n", process_id);
		MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
		run_log<<get_ctime()<<"\tWorker "<<process_id<<" is done working"<<endl;
	}

	return 0;
}

int manager()
{
	int thread_count = 1;
	/*
		Only for test, push file to queue
	*/
	for(int i = 0; i < files_num_to_process; i++){
		char file_dir[FILE_DIR_LENGTH];
		sprintf(file_dir, "Dec+0551_drifting-M19_W_00%d.fits", 18 + i);
		string file_dir_s(file_dir);
		FileInfo fileInfo(file_dir);
		files_queue.push(fileInfo);
		// files_queue.push("Dec+0551_drifting-M19_W_0018.fits");
	}

	for(int i = 0; i < np; i++){
		WorkerHandle tmpWorker(i);
        workers.push_back(tmpWorker);
    }

    for(int i = 1; i < np; i++){
        process_queue.push(i);
    }

	/*
		assign task to workers
	*/
    while(!process_queue.empty()){
        int process_id = process_queue.front();
        int process_status = 0, flag = 0;
		MPI_Request request;
		time_t last_comunic_time = 0, now_time = time(NULL);
        process_queue.pop();

		/* if running time is less than assumed process time, it is considered that processing is not finish, so will not recv the respond */
		if(now_time - workers[process_id].assign_time < TIME_MIN_FOR_PROCESSING){
			process_queue.push(process_id);
			if(HEART_BEAT_STATUS_DEBUG || MANAGER_DEBUG)
				printf("%s\tTime is not up, pushing %d back to queue. now time %ld, ass_time %ld, diff %ld\n", get_ctime(), process_id, now_time, workers[process_id].assign_time, now_time - workers[process_id].assign_time);
			sleep(10 + (TIME_MIN_FOR_PROCESSING - now_time + workers[process_id].assign_time) / np);
			continue;
		}

		/* check the whether worker has finished processing. if not push it back to process_queue; otherwise, handle the report */
        // MPI_Recv(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD, &status);
		MPI_Irecv(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD, &request);
		for(int times = 0; times < TIMES_FOR_RETRY; times++){
			MPI_Test(&request, &flag, &status);
			if(flag == 0){
				if(HEART_BEAT_STATUS_DEBUG || MANAGER_DEBUG)
					printf("%s\tManager notices worker %d not finish for No.%d\n", get_ctime(), process_id, times + 1);
				sleep(1);
			}
			else{
				printf("%s\tManager notices worker %d finished\n", get_ctime(), process_id);
				break;
			}
		}

		/*worker has not finish*/
		if(flag == 0){
			process_queue.push(process_id);
			if(HEART_BEAT_STATUS_DEBUG || MANAGER_DEBUG)
				printf("%s\tManager notices worker %d is not ready, so push it back to queue\n", get_ctime(), process_id);
			continue;
		}

		send_file_name(process_id);
    }
}

int worker()
{
    int process_status = READ_READY;
    MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
    bool something_wrong = false;
	
	/*
		process files
	*/
	char commd[COMMD_LENGTH]; // make directory for tmp results
	sprintf(commd, "mkdir -p %s", dir_to_store_result);
	// system(commd);

    while(process_status != PROCESS_DONE){
        MPI_Recv(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);

        if(process_status == PROCESS_DONE){
            // do nothing
            process_status = PROCESS_EXIT;
            // MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            break;
        }

        /* handle file */
        if(process_status == READY_FOR_FILE){
			char file_name[FILE_DIR_LENGTH];
            MPI_Recv(file_name, FILE_DIR_LENGTH, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);

			// handle files
			process_status = handelFile(file_name);

            if(process_status == FILE_NOT_EXIST || process_status == FILE_DAMAGE){
                process_status = PROCESS_ERROR;
                //MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            }
            else{
				process_status = READ_READY;
                //MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            }
			MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
			printf("%s\tWorker %d send process status to manager\n", get_ctime(), my_rank);
        }
    }

    printf("Jobs of worker %d is done\n", my_rank);
}

int handelFile(char *file_name)
{
    int return_value = 100;
    int process_status = 0;

    printf("Worker %d is handling on %s\n", my_rank, file_name);

	/* handle files */
	sleep(50 + my_rank);

	printf("Worker %d has handled %s\n", my_rank, file_name);
	return process_status;
}

char* get_ctime()
{
	time_t now_time = time(NULL);
	static char time_string[26] = {'\0'};
	strncpy(time_string, ctime(&now_time), 26);
	time_string[24] = '\0';
	return time_string;
}

int test_send()
{
	if(my_rank == 3){
		sleep(4);
	}
	else{
		sleep(13);
	}
	int count = 0;
	MPI_Send(&count, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
	// sleep(1);
	// count++;
	// MPI_Send(&count, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
	printf("%s\tWorker %d send message to manager\n", get_ctime(), my_rank);
}

int test_irecv()
{
	queue<int> q;

	for(int i = 1; i < 2; i++){
		q.push(i);
	}

	while(!q.empty()){
		int i = q.front();
		q.pop();
		int count = 0, flag = 0;
		MPI_Request request;
		MPI_Irecv(&count, 1, MPI_INT, i, tag, MPI_COMM_WORLD, &request);
		// MPI_Test(&request, &flag, &status);
		for(int times = 0; times < TIMES_FOR_RETRY; times++){
			MPI_Test(&request, &flag, &status);
			if(flag == 0){
				printf("%s\tManager notices worker %d not finish for No.%d\n", get_ctime(), i, times + 1);
				sleep(1);
			}
			else{
				printf("%s\tManager notices worker %d finished, message is %d\n", get_ctime(), i, count);
				break;
			}
		}

		/*worker has not finish*/
		if(flag == 0){
			q.push(i);
				printf("%s\tManager notices worker %d is not ready, so push it back to queue\n", get_ctime(), i);
		}
	}
}

int test_irecv_manager()
{
	for(int i = 0; i < np; i++){
		WorkerHandle tmpWorker(i);
        workers.push_back(tmpWorker);
    }

	for(int i = 1; i < np; i++){
        process_queue.push(i);
    }

	while(!process_queue.empty()){
		int i = process_queue.front();
		process_queue.pop();
		int count = 0, flag = 0;
		// MPI_Request request;

		if(workers[i].isPINGed == false){
			MPI_Irecv(&count, 1, MPI_INT, i, tag, MPI_COMM_WORLD, &workers[i].request);
			workers[i].isPINGed = true;
		}

		MPI_Test(&workers[i].request, &flag, &status);
		/*worker has not finish*/
		if(flag == 0){
			process_queue.push(i);
			printf("%s\tManager notices worker %d is not ready, so push it back to queue\n", get_ctime(), i);
		}
		else{
			printf("%s\tManager notices worker %d finished, message is %d\n", get_ctime(), i, count);
			workers[i].isPINGed = false;
			break;
		}

		sleep(TIME_RELAX);
	}
}

int test_recv()
{
	queue<int> q;

	for(int i = 1; i < np; i++){
		q.push(i);
	}

	while(!q.empty()){
		int i = q.front();
		q.pop();
		int process_status = 0, flag = 0;
		MPI_Request request;
		MPI_Recv(&process_status, 1, MPI_INT, i, tag, MPI_COMM_WORLD,&status);
		
		printf("%s\tManager notices worker %d send message %d\n", get_ctime(), i, process_status);
	}
}

int test_sendrecv()
{
	if(my_rank == 2){
		sleep(2);
	}
	int count = my_rank;
	MPI_Sendrecv(&my_rank, 1, MPI_INT, (my_rank + 1) % np, tag, &count, 1, MPI_INT, (my_rank - 1 + np) % np, tag, MPI_COMM_WORLD, &status);
	printf("%s\tWorker %d send message %d to %d and recv from worker %d as %d\n", get_ctime(), my_rank, my_rank, (my_rank + 1) % np, (my_rank - 1 + np) % np, count);
}

int process_routine()
{
	// test_sendrecv();
	if(my_rank == 0){
	    // manager();
		test_irecv_manager();
	}
	else{
	    // worker();
		test_send();
	}

	return 0;
}

int main(int argc, char *argv[])
{
	int provided;
	int start_time = time(NULL);
	MPI_Init_thread(&argc,&argv,MPI_THREAD_MULTIPLE, &provided); // initializes the MPI environment
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); // gets the current process ID
	MPI_Comm_size(MPI_COMM_WORLD, &np); // gets the total number of processes
	MPI_Get_processor_name(node_name, &name_len);

	printf("Process %d of %d is on %s\n", my_rank, np, node_name);

	if(argc == 2 && strcmp(argv[1], "-h") == 0){
		printf("Usage: \n\t./pipeline \n\t-i <Input file dir> \n\t-s <node name where stored file> \n\t-n <process number> \n\t-o <dir to store results>\n");
		return 0;
	}

	int opt = 0;
	while((opt = getopt(argc, argv, "i:n:o:")) != -1) {
        switch(opt) {
            case 'i':
                strncpy(dir_to_process, optarg, FILE_DIR_LENGTH);
                break;
            case 'o':
                strncpy(dir_to_store_result, optarg, FILE_DIR_LENGTH);
                break;
            case 's':
                strncpy(stored_file_node, optarg, FILE_DIR_LENGTH);
                break;
            case 'n':
                files_num_to_process = atoi(optarg);
                break;
			default:
				printf("unknow input arg %c", opt);
				return 1;
        }
    }

	if(my_rank == 0){
		printf("dir_to_process:%s\ndir_to_store_result:%s\nstored_file_node:%s\nprocess_files_num:%d\n", dir_to_process, dir_to_store_result, stored_file_node, files_num_to_process);
	}

	process_routine();
	// test_routine();
	// thread_routine();
	// isend_recv_routine();
	// isend_recv_routine_for_loop();

	//can MPI_BLOCK();
	MPI_Finalize();
	// int end_time = time(NULL);
	// cout<<end_time - start_time<<endl;
	return 0;
}

// yhrun -N 2 -n 4 -p th_mt1 ./mpi-2
// yhrun -N 2 -p th_mt1  mpirun -n 4 -f ./cpi_config ./mpi-2
