#include <mpi.h>
#include <unistd.h>
#include <bits/stdc++.h>
#include <pthread.h>
using namespace std;

#define READ_READY 		0
#define READY_FOR_FILE	1
#define PROCESS_DONE 	-1
#define PROCESS_ERROR	-2
#define PROCESS_EXIT	-3
#define FILE_NOT_EXIST   -4
#define FILE_DAMAGE     -5
#define FILE_DIR_LENGTH 100
#define COMMD_LENGTH 1024
#define TIME_INTERVAL	5	// every time_interval, the manager tries to contact workers
#define TIME_COMMUNICATE_DEADLINE_LINE	TIME_INTERVAL*10	// consider lose contact
#define TIME_RELAX		3
#define TIME_MIN_FOR_PROCESSING	500	// 4 within this time is considerred not finishing process
#define TIME_MAX_FOR_PROCESSING	700	// 10 within this time is considerred normal
#define TIMES_FOR_RETRY	5	// the times that manager trys to contact workers
#define HEART_BEAT_TAG	1
// #define path = dir + file name

/*
for debug
*/
#define THREAD_DEBUG 				false
#define HEART_BEAT_STATUS_DEBUG 	false
#define MANAGER_DEBUG				false
#define WORKER_DEBUG				false

struct FileInfo{
	string file_name;
	bool hasTouched;
	bool isError;
	FileInfo(string _file_name="", bool _hasTouched=false, bool _isError=false): file_name(_file_name), hasTouched(_hasTouched), isError(_isError){}
};

struct WorkerHandle{
	int worker_rank;
	FileInfo file_info;
	long last_comunic_time;
	long assign_time;
	bool isPINGed_jobs;	// whether has been called by manager
	MPI_Request request_jobs;
	bool isPINGed_heartbeat;	// whether has been called by manager
	MPI_Request request_heartbeat;
	WorkerHandle(int _worker_rank, string _file_name="", long _last_comunic_time=time(NULL), long _assign_time=0, bool _isPINGed_jobs=false, bool _isPINGed_heartbeat=false): worker_rank(_worker_rank), last_comunic_time(_last_comunic_time), assign_time(_assign_time), isPINGed_jobs(_isPINGed_jobs), isPINGed_heartbeat(_isPINGed_heartbeat){
		file_info.file_name = _file_name;
	}
};

queue<FileInfo> files_queue;
queue<int> process_queue;

vector<string> process_to_file;
vector<WorkerHandle> workers;
vector<pthread_mutex_t> mutex_locks;

MPI_Status status;

char ip[20] = "127.0.0.1";
char stored_file_node[20] = "TJU-FAST-RFI";
char node_name[FILE_DIR_LENGTH];	// the node that process is running at
char dir_to_process[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set"; // dir in which there are files to be processed
char dir_to_store_result[FILE_DIR_LENGTH] = "/home/YZC/projects/MPI/data_set/processed_results";

ofstream error_log("error.log");
ofstream run_log("run.log");

int files_num_to_process = 0;
int my_rank; // current process ID
int np; // total number of processes
int name_len;
int tag = 0; // communication marks

bool thread_terminate = true;

void* hello(void* rank);
void* manager_thread(void* rank);
void* worker_thread(void* rank);
void* hello_sleep(void* rank);
void* heartbeatTestManager(void* rank);
void* heartbeatTestWorker(void* rank);
int handelFile(char *file_name);
char* get_ctime();

int send_file_name(int process_id)
{
	int process_status;
	if(!files_queue.empty()){
		process_status = READY_FOR_FILE;
		MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);

		char file_name[FILE_DIR_LENGTH];
		strncpy(file_name, files_queue.front().file_name.c_str(), FILE_DIR_LENGTH);
		printf("%s\tManager is assigning %s to worker %d\n", get_ctime(), file_name, process_id);
		MPI_Send(file_name, FILE_DIR_LENGTH, MPI_CHAR, process_id, tag, MPI_COMM_WORLD);

		// process_to_file[process_id] = files_queue.front();
		workers[process_id].file_info = files_queue.front();
		workers[process_id].assign_time = time(NULL);
		files_queue.pop();

		process_queue.push(process_id);
	}
	else{
		process_status = PROCESS_DONE;
		printf("%s\tManager is sending jobs done to worker %d\n", get_ctime(), process_id);
		MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
		run_log<<get_ctime()<<"\tWorker "<<process_id<<" is done working"<<endl;
	}

	return 0;
}

int manager()
{
	int thread_count = 1;
	/*
		Only for test, push file to queue
	*/
	for(int i = 0; i < files_num_to_process; i++){
		char file_dir[FILE_DIR_LENGTH];
		sprintf(file_dir, "Dec+0551_drifting-M19_W_00%d.fits", 18 + i);
		string file_dir_s(file_dir);
		FileInfo fileInfo(file_dir);
		files_queue.push(fileInfo);
		// files_queue.push("Dec+0551_drifting-M19_W_0018.fits");
	}

	/*
		thread start
	*/
	mutex_locks.resize(np);
	pthread_t* thread_handles = NULL;
	thread_handles = (pthread_t*)malloc(thread_count * sizeof(pthread_t));
	thread_terminate = false;

	for(int i = 0; i < np; i++){
		// WorkerHandle tmpWorker(i);
        // workers.push_back(tmpWorker);
		pthread_mutex_init(&mutex_locks[i], NULL);
		// pthread_create(&thread_handles[i], NULL, hello_sleep, (void*)(long)i);
    }

	for(int i = 0; i < thread_count; i++){
		// pthread_mutex_init(&mutex_locks[i], NULL);
		pthread_create(&thread_handles[i], NULL, heartbeatTestManager, (void*)(long)i);
    }

    for(int i = 1; i < np; i++){
        process_queue.push(i);
    }

	/*
		assign task to workers
	*/
    while(!process_queue.empty()){
        int process_id = process_queue.front();
        int process_status = 0, flag = 0;
		// MPI_Request request;
		time_t last_comunic_time = 0, now_time = time(NULL);
        process_queue.pop();

		/* if running time is less than assumed process time, it is considered that processing is not finish, so will not recv the respond */
		if(now_time - workers[process_id].assign_time < TIME_MIN_FOR_PROCESSING){
			process_queue.push(process_id);
			if(HEART_BEAT_STATUS_DEBUG || MANAGER_DEBUG)
				printf("%s\tTime is not up, pushing %d back to queue. now time %s, ass_time %s, diff %ld\n", get_ctime(), process_id, ctime(&now_time), ctime(&workers[process_id].assign_time), now_time - workers[process_id].assign_time);
			sleep(10 + (TIME_MIN_FOR_PROCESSING - now_time + workers[process_id].assign_time) / np);
			continue;
		}

		/* if the worker is not responding for a long enough time, it is considered that the worker is not running normaly, so it will be termimnated */
		pthread_mutex_lock(&mutex_locks[process_id]);
		last_comunic_time = workers[process_id].last_comunic_time;
		pthread_mutex_unlock(&mutex_locks[process_id]);

		if(now_time - last_comunic_time > TIME_COMMUNICATE_DEADLINE_LINE){
			// will not push process_id back to the queue
			// but push file back to file_queue
			files_queue.push(workers[process_id].file_info);
			process_status = PROCESS_DONE;
			MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
			error_log<<get_ctime()<<"\tWorker "<<process_id<<" has been termimnated since not responding"<<endl;
			if(HEART_BEAT_STATUS_DEBUG || MANAGER_DEBUG)
				printf("%s\tWorker %d is not responding, might be down so will be terminated\n", get_ctime(), process_id);
			continue;
		}

		/* if running time is more than assumed process time, it is considered that processing is somehow running unnormaly, so it will be terminated */
		if(workers[process_id].assign_time > 0 && now_time - workers[process_id].assign_time > TIME_MAX_FOR_PROCESSING){
			// will not push process_id back to the queue
			files_queue.push(workers[process_id].file_info);
			process_status = PROCESS_DONE;
			MPI_Send(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD);
			error_log<<get_ctime()<<"\tWorker "<<process_id<<" has been termimnated since running for too much time"<<endl;
			if(HEART_BEAT_STATUS_DEBUG || MANAGER_DEBUG)
				printf("%s\tWorker %d has running for too much time, it will be terminated\n", get_ctime(), process_id);
			sleep(1);
			continue;
		}

		/* check the whether worker has finished processing. if not push it back to process_queue; otherwise, handle the report */
        if(workers[process_id].isPINGed_jobs == false){
			MPI_Irecv(&process_status, 1, MPI_INT, process_id, tag, MPI_COMM_WORLD, &workers[process_id].request_jobs);
			workers[process_id].isPINGed_jobs = true;
		}

		MPI_Test(&workers[process_id].request_jobs, &flag, &status);
		/*worker has not finish*/
		if(flag == 0){
			process_queue.push(process_id);
			if(HEART_BEAT_STATUS_DEBUG || MANAGER_DEBUG)
				printf("%s\tManager notices worker %d is not ready, so push it back to queue\n", get_ctime(), process_id);
			sleep(TIME_RELAX);
			continue;
		}
		else{
			printf("%s\tManager notices worker %d finished\n", get_ctime(), process_id);
			workers[process_id].isPINGed_jobs = false;
		}
		

		printf("%s\tManager is dealing with report from worker %d.\n", get_ctime(), process_id);
		if(process_status == READ_READY && workers[process_id].assign_time != 0){
            run_log<<get_ctime()<<"\t"<<workers[process_id].file_info.file_name<<" has been processed by worker "<<process_id<<endl;
        }

        if(process_status == PROCESS_ERROR){
            if(workers[process_id].file_info.hasTouched){
                // the file has been handled, and error still occurred,
                // so we think the file is damaged and add error message to log file
                error_log<<get_ctime()<<"\tSomething wrong with "<<workers[process_id].file_info.file_name<<endl;
            }
            else{
                // try again
                workers[process_id].file_info.hasTouched = true;
                files_queue.push(workers[process_id].file_info);
            }
        }

		send_file_name(process_id);
    }

	/*
		thread end
	*/
	thread_terminate = true;
	for(int i = 0; i < thread_count; i++){
		pthread_join(thread_handles[i], NULL);
	}

	for(int i = 0; i < np; i++){
		pthread_mutex_destroy(&mutex_locks[i]);
	}

	free(thread_handles);
	thread_handles = NULL;

    // commit log file
    printf("%s\tall procedures are done, committing log files\n", get_ctime());
    run_log<<get_ctime()<<"\t"<<"due to termination of workers, following files have not been processed:"<<endl;
    while(!files_queue.empty()){
        run_log<<"\t\t"<<files_queue.front().file_name<<endl;
        files_queue.pop();
    }

	return 0;
}

int worker()
{
    int process_status = READ_READY;
    MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
	int thread_count = 1;

	/*
		thread start
	*/
	pthread_t* thread_handles = NULL;
	thread_handles = (pthread_t*)malloc(thread_count * sizeof(pthread_t));
	thread_terminate = false;
	for(int i = 0; i < thread_count; i++){
		// pthread_mutex_init(&mutex_locks[i], NULL);
		pthread_create(&thread_handles[i], NULL, heartbeatTestWorker, (void*)(long)i);
    }

	/*
		process files
	*/
	char commd[COMMD_LENGTH]; // make directory for tmp results
	sprintf(commd, "mkdir -p %s", dir_to_store_result);
	// system(commd);

    while(process_status != PROCESS_DONE){
        MPI_Recv(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);

        if(process_status == PROCESS_DONE){
            // do nothing
            process_status = PROCESS_EXIT;
            // MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            break;
        }

        /* handle file */
        if(process_status == READY_FOR_FILE){
			char file_name[FILE_DIR_LENGTH];
            MPI_Recv(file_name, FILE_DIR_LENGTH, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);

			// handle files
			process_status = handelFile(file_name);

            if(process_status == FILE_NOT_EXIST || process_status == FILE_DAMAGE){
                process_status = PROCESS_ERROR;
                //MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            }
            else{
				process_status = READ_READY;
                //MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            }
			MPI_Send(&process_status, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
			printf("%s\tWorker %d send process status to manager\n", get_ctime(), my_rank);
        }
    }

	/*
		thread end
	*/
	printf("%s\tWorker %d starts ending thread\n", get_ctime(), my_rank);
	thread_terminate = true;
	for(int i = 0; i < thread_count; i++){
		pthread_join(thread_handles[i], NULL);
		// pthread_mutex_destroy(&mutex_locks[i]);
	}
	free(thread_handles);
	thread_handles = NULL;

    printf("%s\tJobs of worker %d is done\n", get_ctime(), my_rank);
	return 0;
}

int handelFile(char *file_name)
{
    int process_status = 0;

    printf("%s\tWorker %d is handling on %s\n", get_ctime(), my_rank, file_name);

//	return_value = system("mkdir -p tmp_dir_for_process");
//
//	string tmp_file_dir_for_process = "tmp_dir_for_process";
//
//
//	memset(commd, '\0', COMMD_LENGTH);
//	sprintf(commd, "cd ../unit_test && ~/anaconda3/envs/py37-tf113/bin/python test_whole_pipeline_except_RFI-Net.py --data_dir=%s --file_name=%s --tmp_dir=../utils/%s --process_id=%d", dir_to_process, file_name, tmp_file_dir_for_process.c_str(), my_rank);
//	printf("process %d start to detect RFI\n", my_rank);
//	return_value = system(commd);
//
//	if(return_value == FILE_NOT_EXIST){
//		process_status = FILE_NOT_EXIST;
//	}
//
//	if(return_value == FILE_DAMAGE){
//		process_status = FILE_DAMAGE;
//	}
//
//	memset(commd, '\0', COMMD_LENGTH);
//	sprintf(commd, "mv %s/%s_restored_by_process_%d.h5 %s/%s_processed_by_%d.h5", tmp_file_dir_for_process.c_str(), file_name, my_rank, dir_to_store_result, file_name, my_rank);
//	printf("%s\n", commd);
//	system(commd);
//
//	memset(commd, '\0', COMMD_LENGTH);
//	sprintf(commd, "rm %s/%s*", tmp_file_dir_for_process.c_str(), file_name);
//	printf("%s\n", commd);
//	system(commd);

    if(my_rank < 5){
		sleep(520 + my_rank);
	}

	else{
		sleep(600 + my_rank);
	}

	printf("%s\tWorker %d has handled %s\n", get_ctime(), my_rank, file_name);
	return process_status;
}

void* heartbeatTestManager(void* rank)
{
	long my_thread_rank = (long)rank;
	int tag = HEART_BEAT_TAG, flag = 0;
    MPI_Status status;
	char greeting[FILE_DIR_LENGTH];

	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);
	sprintf(greeting, "Greeting to worker %d", my_rank);

	while(!thread_terminate){
		for(int i = 1; i < np; i++){
			MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, i, tag, MPI_COMM_WORLD);
		}
		if(THREAD_DEBUG)
			printf("%s\tManager send %s\n", get_ctime(), greeting);

		sleep(1);

		for(int i = 1; i < np; i++){

			// MPI_Recv(greeting, FILE_DIR_LENGTH, MPI_CHAR, i, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			// MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, i, tag, MPI_COMM_WORLD, &request);
			// for(int times = 0; times < TIMES_FOR_RETRY; times++){
			// 	MPI_Test(&request, &flag, &status);
			// 	if(flag == 0){
			// 		if(THREAD_DEBUG)
			// 			printf("Thread %ld of manager not received for %d from %d\n", my_thread_rank, times + 1, i);
			// 		sleep(3);
			// 	}
			// 	else{

			// 		pthread_mutex_lock(&mutex_locks[i]);
			// 		workers[i].last_comunic_time = time(NULL);
			// 		pthread_mutex_unlock(&mutex_locks[i]);

			// 		if(THREAD_DEBUG)
			// 			printf("Thread %ld of worker %d received %s at %ld\n", my_thread_rank, my_rank, greeting, workers[i].last_comunic_time);

			// 		times = TIMES_FOR_RETRY;
			// 		break;
			// 	}
			// }

			/* check the whether worker has finished processing. if not push it back to process_queue; otherwise, handle the report */
			if(workers[i].isPINGed_heartbeat == false){
				MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, i, tag, MPI_COMM_WORLD, &workers[i].request_heartbeat);
				workers[i].isPINGed_heartbeat = true;
			}

			MPI_Test(&workers[i].request_heartbeat, &flag, &status);
			/*worker has not finish*/
			if(flag == 0){
				if(THREAD_DEBUG)
					printf("%s\tManager does not receive greeting from worker %d\n", get_ctime(), i);
			}
			else{
				pthread_mutex_lock(&mutex_locks[i]);
				workers[i].last_comunic_time = time(NULL);
				pthread_mutex_unlock(&mutex_locks[i]);

				if(THREAD_DEBUG)
					printf("%s\tManager received greeting from worker %d\n", get_ctime(), i);
				workers[i].isPINGed_heartbeat = false;
			}

			// pthread_mutex_lock(&mutex_locks[i]);
			// workers[i].last_comunic_time = time(NULL);
			// pthread_mutex_unlock(&mutex_locks[i]);

			// printf("thread %d of worker %d received %s at %ld\n", my_thread_rank, my_rank, greeting, workers[i].last_comunic_time);
		}
		sleep(TIME_INTERVAL);
	}
	printf("%s\tFinished thread %ld of worker %d\n", get_ctime(), my_thread_rank, my_rank);
	return NULL;
}

void* heartbeatTestWorker(void* rank)
{
	long my_thread_rank = (long)rank;
	int tag = HEART_BEAT_TAG, flag = 0;
    MPI_Status status;
	char greeting[FILE_DIR_LENGTH];

	// printf("Hello from thread %ld of worker %d\n", my_thread_rank, my_rank);


	while(!thread_terminate){
		// MPI_Recv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		// printf("thread %d of worker %d received %s\n", my_thread_rank, my_rank, greeting);

		// MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &request);
		// for(int times = 0; times < TIMES_FOR_RETRY; times++){
		// 	MPI_Test(&request, &flag, &status);
		// 	if(flag == 0){
		// 		if(THREAD_DEBUG)
		// 			printf("%s\tThread %ld of worker %d not received for No.%d from %d\n", get_ctime(), my_thread_rank, my_rank, times + 1, 0);
		// 		sleep(3);
		// 	}
		// 	else{
		// 		if(THREAD_DEBUG)
		// 			printf("%s\tThread %ld of worker %d received %s\n", get_ctime(), my_thread_rank, my_rank, greeting);

		// 		times = TIMES_FOR_RETRY;
		// 		break;
		// 	}
		// }

		// if(flag != 0){
		// 	// sleep(1);

		// 	sprintf(greeting, "Greeting from thread %ld of worker %d!", my_thread_rank, my_rank);
		// 	MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
		// 	if(THREAD_DEBUG)
		// 		printf("send %s\n", greeting);
		// }

		// if(workers[0].isPINGed_heartbeat == false){
		// 	// MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &workers[my_rank].request_heartbeat);
		// 	// workers[my_rank].isPINGed_heartbeat = true;
		// 	printf("sending false\n");
		// }
		// MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &request);
		// for(int times = 0; times < TIMES_FOR_RETRY; times++){
		// 	MPI_Test(&request, &flag, &status);
		// 	if(flag == 0){
		// 		if(THREAD_DEBUG)
		// 			printf("%s\tThread %ld of worker %d not received for No.%d from %d\n", get_ctime(), my_thread_rank, my_rank, times + 1, 0);
		// 		sleep(3);
		// 	}
		// 	else{
		// 		if(THREAD_DEBUG)
		// 			printf("%s\tThread %ld of worker %d received %s\n", get_ctime(), my_thread_rank, my_rank, greeting);

		// 		times = TIMES_FOR_RETRY;
		// 		break;
		// 	}
		// }

		// if(flag != 0){
		// 	// sleep(1);

		// 	sprintf(greeting, "Greeting from thread %ld of worker %d!", my_thread_rank, my_rank);
		// 	MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
		// 	if(THREAD_DEBUG)
		// 		printf("send %s\n", greeting);
		// 	// workers[my_rank].isPINGed_heartbeat = false;
		// }

		/* check the whether worker has finished processing. if not push it back to process_queue; otherwise, handle the report */
		if(workers[my_rank].isPINGed_heartbeat == false){
			MPI_Irecv(greeting, FILE_DIR_LENGTH, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &workers[my_rank].request_heartbeat);
			workers[my_rank].isPINGed_heartbeat = true;
		}

		MPI_Test(&workers[my_rank].request_heartbeat, &flag, &status);
		/*worker has not finish*/
		if(flag == 0){
			if(THREAD_DEBUG)
				printf("%s\tWorker %d does not receive greeting from manager\n", get_ctime(), my_rank);
		}
		else{
			sprintf(greeting, "Greeting from thread %ld of worker %d!", my_thread_rank, my_rank);
			MPI_Send(greeting, strlen(greeting) + 1, MPI_CHAR, 0, tag, MPI_COMM_WORLD);

			if(THREAD_DEBUG)
				printf("%s\tWorker %d received greeting from manager and send back\n", get_ctime(), my_rank);
			workers[my_rank].isPINGed_heartbeat = false;
		}

		sleep(TIME_INTERVAL);
	}
	printf("%s\tFinished thread %ld of worker %d\n", get_ctime(), my_thread_rank, my_rank);
	return NULL;
}

char* get_ctime()
{
	time_t now_time = time(NULL);
	static char time_string[26] = {'\0'};
	strncpy(time_string, ctime(&now_time), 26);
	time_string[24] = '\0';
	return time_string;
}

int process_routine()
{
	for(int i = 0; i < np; i++){
		WorkerHandle tmpWorker(i);
        workers.push_back(tmpWorker);
    }

	if(my_rank == 0){
	    manager();
	}
	else{
	    worker();
	}

	return 0;
}

int main(int argc, char *argv[])
{
	int provided;
	MPI_Init_thread(&argc,&argv,MPI_THREAD_MULTIPLE, &provided); // initializes the MPI environment
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); // gets the current process ID
	MPI_Comm_size(MPI_COMM_WORLD, &np); // gets the total number of processes
	MPI_Get_processor_name(node_name, &name_len);

	printf("Process %d of %d is on %s\n", my_rank, np, node_name);

	if(argc == 2 && strcmp(argv[1], "-h") == 0){
		printf("Usage: \n\t./pipeline \n\t-i <Input file dir> \n\t-s <node name where stored file> \n\t-n <process number> \n\t-o <dir to store results>\n");
		return 0;
	}

	int opt = 0;
	while((opt = getopt(argc, argv, "i:n:o:")) != -1) {
        switch(opt) {
            case 'i':
                strncpy(dir_to_process, optarg, FILE_DIR_LENGTH);
                break;
            case 'o':
                strncpy(dir_to_store_result, optarg, FILE_DIR_LENGTH);
                break;
            case 's':
                strncpy(stored_file_node, optarg, FILE_DIR_LENGTH);
                break;
            case 'n':
                files_num_to_process = atoi(optarg);
                break;
			default:
				printf("unknow input arg %c", opt);
				return 1;
        }
    }

	if(my_rank == 0){
		printf("dir_to_process:%s\ndir_to_store_result:%s\nstored_file_node:%s\nprocess_files_num:%d\n", dir_to_process, dir_to_store_result, stored_file_node, files_num_to_process);
	}

	process_routine();
	// test_routine();
	// thread_routine();
	// isend_recv_routine();
	// isend_recv_routine_for_loop();

	//can MPI_BLOCK();
	MPI_Finalize();
	// int end_time = time(NULL);
	// cout<<end_time - start_time<<endl;
	return 0;
}

// yhrun -N 2 -n 4 -p th_mt1 ./mpi-2
// yhrun -N 2 -p th_mt1  mpirun -n 4 -f ./cpi_config ./mpi-2
