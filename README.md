# RFI-Pipeline

The RFI-Pipeline is an an extensible RFI detection framework using the [RFI-Net](https://gitee.com/AstroTJU/RFI_Net) as its default RFI detection method. It can run parallel workflow on multi-CPU, multi-GPU cluster to improve data throughput. The framework can schedule workloads based on the running environment, collect results, and handle various types of errors encountered throughout the process. Its main components are replaceable and can be easily customized to the user's needs.

# RFI-Net

The paper of RFI-Net has been published in Monthly Notices of the Royal Astronomical Society (please see [Deep residual detection of radio frequency interference for FAST](https://doi.org/10.1093/mnras/stz3521 "RFI-Net for RFI detection"))

