#!/bin/bash

date +%s.%N #输出程序开始时间
mpirun -np 3 ../build/pipeline-test --dir_to_process ../../data_set --dir_to_store_result ../../data_set/processed_results --workflow_organizer ../utils/workflow.py --py_interpreter ~/anaconda3/envs/astro-py37/bin/python --dir_of_model ../../data_set/saved_models
date +%s.%N #输出程序结束时间
